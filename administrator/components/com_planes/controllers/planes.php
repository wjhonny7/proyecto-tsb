
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class PlanesControllerPlanes extends JControllerLegacy{

	public function nuevo(){

		$view = $this->getView('planes', 'html');
		$view->setLayout('crear');
		$view->display();
	}


	public function save(){

		$app = JFactory::getApplication();
		$input = $app->input;

		// Get the post from data
		$data = $input->getArray( $_POST );

		$planModel = $this->getModel( 'planes' );

		$planModel->instance( $data );

		if ( ! $planModel->save( 'bool' ) ) {
			$app->redirect( 'index.php?option=com_planes&view=planes&layout=crear', 'No se pudo guardar el plan', false );
		}

		$app->redirect( 'index.php?option=com_planes', 'El plan fue guardado correctamente', false );

	}

	public function edit(){

		$id = JRequest::getVar('cid');
		

		$planModel = $this->getModel( 'planes' );

		$plan = $planModel->getObject( $id[0] );

		$view = $this->getView('planes', 'html');

		$view->assignRef('plan', $plan);
		$view->setLayout('crear');
		$view->display();

	}

	public function delete(){

		$cid = JRequest::getVar( 'cid' );

		foreach ($cid as $key => $id) {
			
			$model = $this->getModel( 'planes' );
			$model->instance( $id );

			$model->delete();
		}

		$this->setRedirect( 'index.php?option=com_planes', 'Los planes han sido borrados.' );


	}

	public function publish(){

		$cid = JRequest::getVar( 'cid' );		

		foreach ($cid as $key => $id) {
			
			$model = $this->getModel( 'planes' );
			$model->instance( $id );

			$model->publish();
		}

		$this->setRedirect( 'index.php?option=com_planes' );

	}

	public function cancel(){

		$this->setRedirect('index.php?option=com_planes');
	}
}
?>