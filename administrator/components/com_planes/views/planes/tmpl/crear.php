<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
JHtml::_('behavior.formvalidation');

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addStyleSheet('components/com_planes/assets/css/style.css');

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'planes.cancel' || document.formvalidator.isValid(document.id('plan-form'))) {
			Joomla.submitform(task, document.getElementById('plan-form'));
		}
	}
</script>


<form name="adminForm" id="plan-form" method="post" action="<?php echo JRoute::_('index.php?option=com_planes');?>" class="form-validate">
	<fieldset>
		<legend>Nuevo plan</legend>
		<div class="form-plan">
			<ul>
				<li><label for="titulo">Titulo: </label><input type="text" name="titulo" class="inputbox required" value="<?php echo $this->plan->titulo ?>" aria-required="true" required="required" aria-invalid="true"/></li>
				<li><label for="caracteristicas">Características: </label><textarea name="caracteristicas" ><?php echo $this->plan->caracteristicas ?></textarea></li>
				<li><label for="duracion">Tiempo en meses: </label><input type="text" name="duracion" value="<?php echo $this->plan->duracion ?>" required="required" class="inputbox required" placeholder="(Entre 1 y 12 Meses)" aria-required="true" aria-invalid="true"/></li>
				<li><label for="precio">Precio: </label><input type="text" name="precio" value="<?php echo $this->plan->precio ?>" required="required" class="inputbox required" aria-required="true" placeholder="Ej: 10000" aria-invalid="true"/></li>
				<li>
					<label for="tipo_plan">Tipo de plan: </label>
					<select name="tipo_plan">
						<option>Seleccione</option>
						<option value="1">Anunciante</option>
						<option value="2">Socio franquicia</option>
					</select>
				</li>
				<li><label for="descuento">Descuento: </label><input type="text" name="descuento" value="<?php echo $this->plan->descuento ?>" required="required" class="inputbox required" aria-required="true" aria-invalid="true" placeholder="Ej: 10"/></li>

			</ul>

			<input type="hidden" name="id" value="<?php echo $this->plan->id ?>" />
		</div>

	</fieldset>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option" value="com_planes" />
</form>