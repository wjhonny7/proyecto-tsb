<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$loggeduser = JFactory::getUser();
?>

<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search">Buscar planes:</label>
			<input type="text" name="titulo" id="titulo" value="<?php echo $this->escape($this->state->get('filter.titulo')); ?>" title="Titulo del plan" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('titulo').value='';this.form.submit();"> Limpiar</button>
		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="center">
					<?php echo JHtml::_('grid.sort', 'Titulo', 'titulo', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap" width="10%">
					<?php echo JHtml::_('grid.sort', 'Tiempo en meses', 'duracion', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap" width="20%">
					Características
				</th>
				<th class="nowrap" width="5%">
					Precio
				</th>
				<th class="nowrap" width="10%">
					Tipo del plan
				</th>
				<th class="nowrap" width="15%">
					Descuento
				</th>
				<th class="nowrap" width="3%">
					<?php echo JHtml::_('grid.sort', 'Id', 'id', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap" width="3%">Publicar</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->planes as $i => $item) :
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td class="center">
					<?= $item->titulo ?>
				</td>
				<td class="center">
					<?= $item->duracion ?>
				</td>
				<td class="center">
					<?= $item->caracteristicas ?>
				</td>
				<td class="center">
					<?= $item->precio ?>
				</td>
				<td class="center">
					<?php echo ($item->tipo_plan == 1) ? "Anunciante" : "Socia franquicia";  ?>
				</td>
				<td class="center">
					<?= $item->descuento ?>%
				</td>
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
				<td class="center"><?php echo JHtml::_('grid.boolean', $i, $item->estado, 'planes.publish', 'planes.publish'); ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
