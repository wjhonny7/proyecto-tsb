<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class PlanesViewPlanes extends JViewLegacy {

	protected $planes;
	protected $plan;
	protected $state;
	protected $pagination;
	
	// Function that initializes the view
	function display( $tpl = null ){

		$this->planes = $this->get('Objects');
		$this->state = $this->get('State');
		$this->pagination = $this->get('Pagination');

	
		// Add the toolbar with the actions
		$this->addToolbar();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::title( "Gestión de Planes", 'install' );

		$layout = $this->getLayout();

		if ($layout != 'default') {
			JToolBarHelper::cancel( "planes.cancel" );
			JToolBarHelper::divider();
			JToolBarHelper::save("planes.save");

		}else{
			JToolBarHelper::addNew( "planes.nuevo" );
			JToolBarHelper::divider();
			JToolBarHelper::editList( "planes.edit" );
			JToolBarHelper::divider();
			JToolBarHelper::trash( "planes.delete" );
		}
	}

}
?>