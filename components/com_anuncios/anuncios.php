
<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS .  'uploader.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS .  'helper.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS .  'pagos.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS .  'misc.php' );
require_once( JPATH_COMPONENT . DS . 'models' . DS . 'default.php' );
require_once( JPATH_COMPONENT . DS . 'models' . DS . 'anuncio.php' );




$controller = JControllerLegacy::getInstance('Anuncios');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>