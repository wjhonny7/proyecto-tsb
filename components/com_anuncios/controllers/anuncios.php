<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class AnunciosControllerAnuncios extends JController{


	public function selectSubcategorias(){

		$response = new stdClass();

		$id = JRequest::getInt( 'id' );

		$model = $this->getModel( 'categorias' );

		$subcategorias = $model->getSubcategorias( $id );

		$subs = array();

		foreach ( $subcategorias as $key => $subcategoria ) {
			
			$childs = $model->getSubcategorias( $subcategoria->id );

			if ( empty( $childs )) {
				array_push( $subs, $subcategoria );
			} else {

				foreach ( $childs as $key => $children ) {
					$children->category_name = $subcategoria->category_name . '-' .$children->category_name;
				}

				$subs = array_merge( $subs, $childs );
			}

		}


		if ( empty( $subs ) ) {
			$response->status = 500;
			$response->message = 'La categoria no tiene subcategorias';
			echo json_encode($response);
			die;
		}

		
		$response->categorias = $subs;
		echo json_encode($response);
		die;
	}

	public function getPlanes(){

		$response = new stdClass();

		$planModel = $this->getModel( 'planes' );

		$wheres = array(

			0 => (object)array(
				'key' => 'estado',
				'value' => '1',
				'condition' => '='
			)
		);

		$plan = $planModel->getObjects( $wheres );

		$response->planes = $plan;
		echo json_encode($response);
		die;


	}
	
	public function save(){

		$data = JRequest::getVar('data');
		$response = new stdClass();

		$modelPlan = $this->getModel( 'planes' );

		$plan = $modelPlan->getObject( (int) $data['plan'] );

		$costoAnuncio = (int) $plan->precio;


		if ( $plan->descuento != '0') {
			$costoAnuncio = (int) $plan->precio * ( (int) $plan->descuento / 100 );
		}

		if( !empty($data['enlace']) ){
			$descuento = (float) $data['enlace'];

			$costoAnuncio = $costoAnuncio * $descuento;
		}

		$producto = $this->getModel('producto');

		$user = JFactory::getUser();

		if ( $data['id_user'] != 'Usuarios' ) {
			$idUser = $data['id_user'];
		}else{
			$idUser = $user->get('id');	
		}


		$date = date( 'Y-m-d H:i:s' );

		$sku = md5(uniqid());

		$idProducto = ! empty($data[ 'id' ]) ? (int)$data[ 'id' ] : null;



		$args = array(
				'virtuemart_product_id' => $idProducto
			,	'product_available_date' => $date
			,	'modified_on' => $date
			,	'modified_by' => $idUser
			,	'created_by' => $idUser
			,	'razon_social' => $data['razon_social']
			,	'descuento_tsb' => $data['descuento']
			,	'direccion' => $data['direccion']
			,	'coordenadas' => $data['coordenadas']
			,	'plan' => $data['plan']
		);

		if( ! is_numeric( $idProducto )  ){
 		 	$args[ 'published' ] = '0';
 		 	$args[ 'product_sku' ] = $sku;
 		}


		// si el anuncio no se edita, quiere decir que es nuevo y se llenan los campos de fecha de creación
 		if( !is_numeric($idProducto) ){
 			$args = array_merge( $args, array(
 					'created_on' => $date
				));
 		}

		$producto->instance( $args );

		$slugProducto = Misc::parseNomField( $data['titulo'] );

		$args_ext = array(
				'virtuemart_product_id' =>	$idProducto
			,	'product_s_desc' => $data['descripcion_corta']
			,	'product_desc' => $data['descripcion_detallada']
			,	'product_name' => $data['titulo']
			,	'slug' => $slugProducto
		);


		//se procede a guardar el anuncio
 		if( $producto->save($args_ext, 'bool') ){

 			$idProducto = empty($producto->insertId) ? $idProducto : $producto->insertId;

 			foreach ($data['subcategorias'] as $key => $subcategoria) {

				$findme   = '-';
				$pos = strpos($subcategoria, $findme);

				if ( $pos === false) {

					$idCategoria = $producto->getCategoria( $subcategoria );

					$productoCategoria = array(
						$idProducto,
						$idCategoria->virtuemart_category_id
					);

					$producto->mergeCategoria( $productoCategoria );

				}else{

					$name = explode( '-', $subcategoria );

					$idCategoria = $producto->getCategoria( $name[0] );

					$idSubcategoria = $producto->getSubcategoria( $idCategoria->virtuemart_category_id, $name[1] );

					$productoCategoria = array(
						$idProducto,
						$idSubcategoria->virtuemart_category_id
					);

					$producto->mergeCategoria( $productoCategoria );

				}

			
			}

			$imagenes = $this->saveImages( $data['imagenes'], $date, $idUser );


			$this->mergeImagenes( $idProducto, $imagenes );

 			//se guarda el precio del anuncio
 			$id_productprice = !empty($data['virtuemart_product_price_id']) ? $data['virtuemart_product_price_id'] : null;

 			$args_price = array(
 					'virtuemart_product_price_id' => $id_productprice
 				,	'virtuemart_product_id' => $idProducto
 				,	'product_price' => $data['precio']
 				,	'modified_on' => $date
 				,	'modified_by' => $idUser
  			);
  			
  			if( !is_numeric($id_productprice) ){
				$args_price = array_merge( $args_price, array(
						'created_on' => $date
					,	'created_by' => $idUser
				));
			}

 			$producto->savePriceProduct($args_price, 'bool');

 		}

		if ( $costoAnuncio != 0 ) {
			$mail = $user->get('email');

			$pagos = new AnuncioPagoHelper();

			$args = array(
					'refVenta' => $sku
				,	'description' => 'Compra Anuncio'
				,	'valor' => $costoAnuncio
				,	'iva' => 0
				,	'basevalor' => 0
				,	'comprador_email' => $mail
				,	'currency' => 'COP'
				,	'url_respuesta' => JURI::root().'index.php/anuncios?layout=listar'
				,	'url_confirmacion' => JURI::root().'index.php/anuncios?task=anuncios.anuncio'
			);

			$pagos->instance( $args );


			$formulario = $pagos->getForm();

			$response->form = $formulario;
			$response->status = 200;
			$response->message = 'Anuncio guardado correctamente. Redirigiendo a plataforma de pagos...';

			echo json_encode($response);
			die;

		}else{

			$response->status = 200;
			$response->message = 'Anuncio guardado correctamente.';

			echo json_encode($response);
			die;
		}
		

	
		

	}

	public function saveImages( $imagenes, $date, $user ){

		$return = new stdClass();
		$return->idsImages = array();

		foreach ($imagenes as $key => $imagen) {
			
			$modelImagen = $this->getModel('medias');			

			$urlImagen = 'imganuncios/tmp/' . $imagen;

			$type = explode( ".", $imagen );

			$type = 'image/' . $type[1];

			$productoImagen = array(
					'file_title' => $imagen
				,	'file_mimetype' => $type
				,	'file_type' => 'product'
				,	'file_url' => 'images/stories/virtuemart/product/'.$imagen
				,	'created_on' => $date
				,	'created_by' => $user
				,	'modified_on' => $date
				,	'modified_by' => $user
			);

			$modelImagen->instance( $productoImagen );

			if ( $modelImagen->save('bool')) {

				$pathOficial = 'images/stories/virtuemart/product/' . $imagen;

				rename( $urlImagen, $pathOficial );

				unlink( $urlImagen ); 
			}

			$idImagen = $modelImagen->insertId;
			$return->idsImages[] = $idImagen;

			
		}

		return $return;

	}

	public function mergeImagenes( $idProducto, $imagenes ){

		foreach ($imagenes->idsImages as $key => $imagen) {
			
			$modelImagen = $this->getModel( 'medias' );

			$args = array(
				$idProducto,
				$imagen
			);

			$modelImagen->mergeImagenes( $args );

		}
	}

	/**
	*
	* Uploads and parse and Image file
	*
	*/
	public function uploadImages(){


		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'jpg', 'JPG', 'png', 'jpeg' );
		// max file size in bytes
		$sizeLimit = 3 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('imganuncios/tmp/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}

	/**
	*
	* Eliminar una imagen
	*
	*/
	public function deleteImage(){

		$requestData = JRequest::getVar( 'data', array() );		
		$response = new stdClass();
		$model = $this->getModel('anuncios');
		$mediasModel = $this->getModel( 'medias' );
		$pathImg = 'imganuncios/tmp/'.$requestData[ 'filename' ];
		$pathVM = 'images/stories/virtuemart/product/'.$requestData[ 'filename' ];

		$thumb = explode( '.', $requestData['filename'] );
		$thumb = $thumb[0] . '_90x90' . $thumb[ 1 ];

		$pathVMThumbs = 'images/stories/virtuemart/product/' . $thumb;

		if( file_exists($pathImg) )
			unlink($pathImg);
			
		if( file_exists($pathVM) )
			unlink($pathVM);

		if( file_exists($pathVMThumbs) )
			unlink($pathVMThumbs);


		$image = $mediasModel->selectMediaByFile( $requestData['filename'] );

		$mediasModel->deleteImage( $image->virtuemart_media_id );

		$model->deleteImageBy( 'file_title', $requestData['filename'] );

		$response->success = true;
		$response->filename = $requestData[ 'filename' ];
		$response->imgindex = $requestData[ 'imgindex' ];

		echo json_encode($response);
		die;
	}

	public function anuncio(){

        $metodo_pago = $_REQUEST['payment_method_type'];
		$monto = $_REQUEST['value'];
		$dateTransaction = $_REQUEST['transaction_date'];
		$emailComprador = $_REQUEST['email_buyer'];
		$referenciaVenta = $_REQUEST['reference_sale'];


		if($_REQUEST['state_pol'] == 4){

			$productoModel = $this->getModel( 'producto' );

			$anuncio =  $productoModel->getAnuncioBySku( $referenciaVenta );

			$meses = (int) $anuncio->duracion * 30;

			$dateExpira = date( 'Y-m-d', strtotime( $meses .' days' ) );

			$productoModel->publishAnuncio( $referenciaVenta, $dateExpira );

			$message = "La transacción se realizó correctamente";

		}else{
			$message = "Hubo un error cuando se trato de realizar la transacción";
		}

        $mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-mail.jpg'><br><br><br><br>";
        $contenido .= "Fecha de transacción: ".$dateTransaction."<br>";
        $contenido .= "Correo electronico del comprador: ".$emailComprador."<br>";
       	$contenido .= "Monto Pagado: ".$monto."<br>";
       	$contenido .= "Observaciones: ".$message."<br>";
        $contenido .= "<img src='".JURI::root()."images/footer-mail.jpg'></div>";

        $mail->setSender( 'info@todosobrefundaciones.com' );
        $mail->addRecipient( $emailComprador );
        $mail->setSubject( "Usted ha realizado una transacción a través de PayU" );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );


        // enviar email
        $mail->Send();
        
   	}

   	public function unpublish(){

   		$id = JRequest::getInt( 'id' );

   		$response = new stdClass();

   		$productoModel = $this->getModel( 'producto' );

   		$producto = $productoModel->unpublish( $id );

   		if ( ! $producto ) {

   			$response->status = 500;
   			$response->message = 'No se pudo despublicar el anuncio, intentelo más tarde';
   			echo json_encode($response);
			die;
   		}

   		$response->status = 200;
   		$response->message = 'Anuncio despublicado correctamente';
   		echo json_encode($response);
		die;
   	}

   	public function publish(){

   		$id = JRequest::getInt( 'id' );

   		$response = new stdClass();

   		$productoModel = $this->getModel( 'producto' );

   		$producto = $productoModel->publish( $id );

   		if ( ! $producto ) {

   			$response->status = 500;
   			$response->message = 'No se pudo publicar el anuncio, intentelo más tarde';
   			echo json_encode($response);
			die;
   		}

   		$response->status = 200;
   		$response->message = 'Anuncio publicado correctamente';
   		echo json_encode($response);
		die;
   	}

   	public function unpublishAnuncios(){

   		$ids = JRequest::getVar( 'ids' );
   		$response = new stdClass();

   		foreach ($ids as $key => $id) {
   			$productoModel = $this->getModel( 'producto' );

   			$producto = $productoModel->unpublish( $id );

   			if ( ! $producto ) {

	   			$response->status = 500;
	   			$response->message = 'No se pudieron despublicar los anuncios, intentelo más tarde';
	   			echo json_encode($response);
				die;
   			}
   		}


   		$response->status = 200;
   		$response->message = 'Anuncios despublicados correctamente';
   		echo json_encode($response);
		die;
   	}

   	public function publishAnuncios(){

   		$ids = JRequest::getVar( 'ids' );
   		$response = new stdClass();

   		foreach ($ids as $key => $id) {
   			$productoModel = $this->getModel( 'producto' );

   			$producto = $productoModel->publish( $id );

   			if ( ! $producto ) {

	   			$response->status = 500;
	   			$response->message = 'No se pudieron publicar los anuncios, intentelo más tarde';
	   			echo json_encode($response);
				die;
   			}
   		}


   		$response->status = 200;
   		$response->message = 'Anuncios publicados correctamente';
   		echo json_encode($response);
		die;
   	}

   	public function deleteAnuncio(){

   		$id = JRequest::getVar( 'id' );
   		$response = new stdClass();

		$productoModel = $this->getModel( 'producto' );
		$mediasModel = $this->getModel( 'medias' );

		$images = $mediasModel->getMediasById( $id );

		foreach ($images as $key => $media) {

			$imagen = $mediasModel->selectMedia( $media->virtuemart_media_id );

			$pathVMThumbs = 'images/stories/virtuemart/product/' . $imagen->file_title;

			if( file_exists($pathVMThumbs) )
				unlink($pathVMThumbs);

			$mediasModel->deleteMedia( $media->virtuemart_media_id );
			# code...
		}

		$producto = $productoModel->deleteAnuncio( $id );

		if ( ! $producto ) {

			$response->status = 500;
			$response->message = 'No se pudo eliminar el anuncio, intentelo más tarde';
			echo json_encode($response);
			die;
		}
   		
   		$response->status = 200;
   		$response->message = 'Anuncio eliminado correctamente';
   		echo json_encode($response);
		die;
   	}

   	public function reactivarPlan(){

   		$data = JRequest::getVar( 'data' );
   		$response = new stdClass();

   		$productoModel = $this->getModel( 'producto' );
   		$planModel = $this->getModel( 'planes' );

   		$anuncio = $productoModel->getAnuncioById( $data['id'] );
   		$plan = $planModel->getObject( (int) $data['plan'] );

   		$costoAnuncio = (int) $plan->precio;

		if ( $plan->descuento != '0') {
			$costoAnuncio = (int) $plan->precio * ( (int) $plan->descuento / 100 );
		}

   		$sku = $anuncio->product_sku;

   		$pagos = new AnuncioPagoHelper();

   		$user = JFactory::getUser();

   		$mail = $user->get('email');

		$args = array(
				'refVenta' => $sku
			,	'description' => 'Compra Anuncio'
			,	'valor' => $costoAnuncio
			,	'iva' => 0
			,	'basevalor' => 0
			,	'comprador_email' => $mail
			,	'currency' => 'COP'
			,	'url_respuesta' => JURI::root().'index.php/anuncios?layout=listar'
			,	'url_confirmacion' => JURI::root().'index.php/anuncios?task=anuncios.anuncio'
		);

		$pagos->instance( $args );

		$formulario = $pagos->getForm();

		$response->form = $formulario;
   		$response->status = 200;
   		$response->message = 'Redirigiendo a la plataforma de pagos.';
   		echo json_encode($response);
		die;

   	}

   	public function getAnuncio(){

   		$id = JRequest::getVar('id');

   		$anuncioModel = $this->getModel( 'anuncios' );
   		$mediasModel = $this->getModel( 'medias' );

		
   		$anuncio = $anuncioModel->getAnuncioById( $id );
   		$categorias = $anuncioModel->getCategoriesById( $id );
   		$images = $mediasModel->getMediasById( $id );

   		$imagenes = array();

   		foreach ($images as $key => $image) {

   			$imagen = $mediasModel->selectMedia( $image->virtuemart_media_id );

   			array_push($imagenes, $imagen->file_title);
   		}

   		$categories = array();

   		foreach ($categorias as $key => $categoria) {

   			$categoriaPadre = $anuncioModel->getSubcategoriasById( $categoria->virtuemart_category_id );

   			foreach ($categoriaPadre as $key => $category) {

   				$padres = $anuncioModel->getPadres( $category->category_parent_id );

   				foreach ($padres as $key => $padre) {
   					$categoria->category_name = $padre->category_name .'-'.$categoria->category_name;

   					array_push( $categories, $categoria->category_name );
   				}

   			}	
   		}

   		$view = $this->getView('anuncios','html');

   		$view->assignRef( 'anuncio', $anuncio );
   		$view->assignRef( 'images', $imagenes );
   		$view->assignRef( 'categorias', $categories );

   		$view->setLayout( 'edit' );

   		$view->display();


   	}

   	public function vencimientoProducto(){

   		$productoModel = $this->getModel('producto');

   		$productos = $productoModel->getObjects();

   		$date = date( 'Y-m-d' );

   		foreach ($productos as $key => $producto) {

   			if ($date >= $producto->fecha_vencimiento) {
   				
   				$args = array('virtuemart_product_id' => $producto->virtuemart_product_id, 'published' => '0' );

   				$productoModel->instance( $args );

   				$productoModel->save();
   				
   			}
   		}
   	}

	
	
}
?>