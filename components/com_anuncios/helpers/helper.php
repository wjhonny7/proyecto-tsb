<?php
/**
 * Helper class for anuncios component
 *
 * 
 */
class AnunciosHelper {

    public static function getUsersName()  {

		$db = JFactory::getDbo();

    	$query = $db ->getQuery(true);
    	$query->select('id as value, name as text');
    	$query->from('#__users');
    	// $query->innerJoin( '#__users' );
    	// $query->where( 'b.category_parent_id = 7' );
    	
    	$db->setQuery( $query );

    	return $db->loadObjectList();
    }

    public static function getPlanes()  {

		$db = JFactory::getDbo();

    	$query = $db ->getQuery(true);
    	$query->select('id as value, titulo as text');
    	$query->from('#__planes');
    	$query->where( 'estado = 1' );
        $query->where( 'tipo_plan = 2' );
    	
    	$db->setQuery( $query );

    	return $db->loadObjectList();
    }

    public static function getPlanesAnunciante()  {

        $db = JFactory::getDbo();

        $query = $db ->getQuery(true);
        $query->select('id as value, titulo as text');
        $query->from('#__planes');
        $query->where( 'estado = 1' );
        $query->where( 'tipo_plan = 1' );
        
        $db->setQuery( $query );

        return $db->loadObjectList();
    }

     public static function getCategories()  {

		$db = JFactory::getDbo();

    	$query = $db ->getQuery(true);
    	$query->select('a.*, b.*');
    	$query->from('#__virtuemart_categories_es_es AS a');
    	$query->innerJoin( '#__virtuemart_category_categories AS b ON b.category_child_id = a.virtuemart_category_id' );
    	$query->where( 'b.category_parent_id = 0' );
    	
    	$db->setQuery( $query );

    	return $db->loadObjectList();
    }

    public static function getUser()  {

        $db = JFactory::getDbo();

        $user = JFactory::getUser();


        $query = $db ->getQuery(true);
        $query->select('*');
        $query->from('#__users');
        $query->where( 'id = '. $db->quote( $user->id ) );
        
        $db->setQuery( $query );

        return $db->loadObject();
    }


}
?>