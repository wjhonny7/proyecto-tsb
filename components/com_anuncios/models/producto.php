<?php
/**
 * @author Sainet Ingenieria Ltda.
 * @author Robinson Perdomo
 * @link http://creandopaginasweb.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

/**
 * AdWizard Anuncios Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_anuncios
 */

jimport('joomla.application.component.model');

// Initializes the Class
class AnunciosModelProducto extends AnunciosModelDefault{


	/**
	 * Object Id
	 * @var int
	 */
	public $virtuemart_product_id;
	/**
	* Attribute
	* @var Int
	*/
	public $virtuemart_vendor_id;
	/**
	* Attribute
	* @var Int
	*/
	public $product_parent_id;
	/**
	* Attribute
	* @var String
	*/
	public $product_sku;
	/**
	* Attribute
	* @var String
	*/
	public $product_weight;
	/**
	* Attribute
	* @var String
	*/
	public $product_weight_uom;
	/**
	* Attribute
	* @var String
	*/
	public $product_length;
	/**
	* Attribute
	* @var String
	*/
	public $product_width;
	/**
	* Attribute
	* @var String
	*/
	public $product_height;
	/**
	* Attribute
	* @var String
	*/
	public $product_lwh_uom;
	/**
	* Attribute
	* @var String
	*/
	public $product_url;
	/**
	* Attribute
	* @var String
	*/
	public $product_in_stock;
	/**
	* Attribute
	* @var String
	*/
	public $product_ordered;
	/**
	* Attribute
	* @var String
	*/
	public $low_stock_notificatio;
	/**
	* Attribute
	* @var String
	*/
	public $product_available_date;
	/**
	* Attribute
	* @var String
	*/
	public $product_availability;
	/**
	* Attribute
	* @var String
	*/
	public $product_special;
	/**
	* Attribute
	* @var String
	*/
	public $product_sales;
	/**
	* Attribute
	* @var String
	*/
	public $product_unit;
	/**
	* Attribute
	* @var String
	*/
	public $product_packaging;
	/**
	* Attribute
	* @var String
	*/
	public $product_params;
	/**
	* Attribute
	* @var String
	*/
	public $hits;
	/**
	* Attribute
	* @var String
	*/
	public $intnotes;
	/**
	* Attribute
	* @var String
	*/
	public $metarobot;
	/**
	* Attribute
	* @var String
	*/
	public $metaauthor;
	/**
	* Attribute
	* @var String
	*/
	public $layout;
	/**
	* Attribute
	* @var String
	*/
	public $published;
	/**
	* Attribute
	* @var String
	*/
	public $pordering;
	/**
	* Attribute
	* @var Data
	*/
	public $created_on;
	/**
	* Attribute
	* @var Int
	*/
	public $created_by;
	/**
	* Attribute
	* @var Data
	*/
	public $modified_on;
	/**
	* Attribute
	* @var Int
	*/
	public $modified_by;
	/**
	* Attribute
	* @var String
	*/
	public $locked_on;
	/**
	* Attribute
	* @var String
	*/
	public $locked_by;
	/**
	* Attribute
	* @var String
	*/	
	public $fecha_vencimiento;
	/**
	* Attribute
	* @var String
	*/

	public $razon_social;
	/**
	* Attribute
	* @var String
	*/

	public $descuento_tsb;
	/**
	* Attribute
	* @var String
	*/
	public $direccion;
	/**
	* Attribute
	* @var String
	*/

	public $coordenadas;
	/**
	* Attribute
	* @var String
	*/

	public $plan;
	/**
	* Attribute
	* @var String
	*/	

	public $table = "#__virtuemart_products";

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__virtuemart_products';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'adwizard.state.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'virtuemart_product_id'
		,	'published'
		,	'virtuemart_vendor_id'
		,	'product_parent_id'
		,	'product_sku'
		,	'product_weight'
		,	'product_weight_uom'
		,	'product_length'
		,	'product_width'
		,	'product_height'
		,	'product_lwh_uom'
		,	'product_url'
		,	'product_in_stock'
		,	'product_ordered'
		,	'low_stock_notificatio'
		,	'product_available_date'
		,	'product_availability'
		,	'product_special'
		,	'product_sales'
		,	'product_unit'
		,	'product_packaging'
		,	'product_params'
		,	'hits'
		,	'intnotes'
		,	'metarobot'
		,	'metaauthor'
		,	'layout'
		,	'pordering'
		,	'created_on'
		,	'created_by'
		,	'modified_on'
		,	'modified_by'
		,	'locked_on'
		,	'locked_by'
		,	'fecha_vencimiento'
		,	'razon_social'
		,	'descuento_tsb'
		,	'direccion'
		,	'coordenadas'
		,	'plan'
	);

	/**
	 * Attributes Map
	 * @var array
	 */
	var $extra_map = array(

			'product_name'
		,	'product_desc'
		,	'product_s_desc'
		,	'product_price'
		,	'slug'
	);


	
	/**
	 * Methods
	 * 
	 */

	/**
	* Override AnunciosModelDefault::save y extender el guardado para otras tablas
	* @param{ Array } $args_ext argumentos extras para guardar en la tabla #__virtuemart_products_es_es
	*/
	public function save( $args_ext = null, $return = 'string' ){

		if( parent::save('bool') ){

			return $this->saveProductExt( $args_ext, $return );
		}
	}

	/**
	* Guardar en la tabla #__virtuemart_products_es_es
	* @param{ Array } $args_ext argumentos extras para guardar en la tabla #__virtuemart_products_es_es
	*/
	protected function saveProductExt( $args_ext = null, $return = 'string' ){

		if( !is_array($args_ext) || empty($args_ext) )
			return false;

		// Instance database
		$db = JFactory::getDbo();
		$response = new stdClass();
		$model = new stdClass();		

		// Fetch the model attributes with the $model's var
		foreach ( $args_ext as $campo => $valor ) {
			$model->$campo = $valor;
		}
		unset( $valor );


		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( ! is_numeric( $model->virtuemart_product_id )  ){

			$model->virtuemart_product_id = empty($this->insertId) ? $this->virtuemart_product_id : $this->insertId;			


			if (! $db->insertObject( '#__virtuemart_products_es_es', $model, 'virtuemart_product_id' ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( '#__virtuemart_products_es_es', $model, 'virtuemart_product_id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}
	}

	/**
	* guardar el precio del anuncio
	*/
	public function savePriceProduct( $args_ext, $return = 'string' ){

		if( !is_array($args_ext) && empty($args_ext) )
			return false;

		// Instance database
		$db = JFactory::getDbo();
		$response = new stdClass();
		$model = new stdClass();		

		// Fetch the model attributes with the $model's var
		foreach ( $args_ext as $campo => $valor ) {
			$model->$campo = $valor;
		}
		unset( $valor );

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->virtuemart_product_price_id == NULL || $model->virtuemart_product_price_id == "" ){

			if (! $db->insertObject( '#__virtuemart_product_prices', $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( '#__virtuemart_product_prices', $model, 'virtuemart_product_price_id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}
	}


	public function saveNombre( $args ){


		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->insert('#__virtuemart_products_es_es');
		$query->values( $db->quote($args[0]).", ".$db->quote($args[1], true).", ".$db->quote($args[2], true).",". $db->quote($args[3]).",'','','',".$db->quote($args[4] ).",".$db->quote($args[5]).",".$db->quote($args[6]).",".$db->quote($args[7]).",".$db->quote($args[8]).",".$db->quote($args[9]) );

		$db->setQuery( $query );
		
		return $db->execute();

	}

	public function mergeCategoria( $args ){

		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		

		$query->insert('#__virtuemart_product_categories');
		$query->values("'','".$args[0]."','".$args[1]."',''");

		$db->setQuery( $query );
		
		return $db->execute();

	}


	public function mergePrecio( $args ){

		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		

		$query->insert('#__virtuemart_product_prices');
		$query->values("'','".$args[0]."','','".$args[1]."','0','0.00000','0','0','31','','','0','0','".$args[2]."','".$args[3]."','".$args[2]."','".$args[3]."','','0'");

		$db->setQuery( $query );
		
		return $db->execute();

	}

	public function publishAnuncio( $sku, $date ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products'  );
		$query->set( 'published = 1 , fecha_vencimiento ='.$db->quote($date) );
		$query->where( 'product_sku = '. $db->quote($sku) );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}

	public function unpublish( $id ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products' );
		$query->set( 'published = 0' );
		$query->where( 'virtuemart_product_id = '. $db->quote($id) );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}

	public function deleteAnuncio( $id ){
		
		// Instance databse
		$db = JFactory::getDbo();

		$query = ("DELETE a, b, c, d, e FROM jos_virtuemart_products AS a, jos_virtuemart_products_es_es AS b, jos_virtuemart_product_categories AS c, jos_virtuemart_product_prices AS d, jos_virtuemart_product_medias AS e WHERE b.virtuemart_product_id = a.virtuemart_product_id AND c.virtuemart_product_id = a.virtuemart_product_id AND d.virtuemart_product_id = a.virtuemart_product_id AND e.virtuemart_product_id = a.virtuemart_product_id AND a.virtuemart_product_id = '".$id."'");
		
		$db->setQuery( $query );
		
		return $db->execute();
	}

	public function publish( $id ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products' );
		$query->set( 'published = 1' );
		$query->where( 'virtuemart_product_id = '. $db->quote($id) );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}


	public function getCategoria( $categoria ){

		if( ! is_string( $categoria ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*'  );
		$query->from( '#__virtuemart_categories_es_es' );
		$query->where( 'category_name = '. $db->quote($categoria) );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
	}

	public function getSubcategoria( $id, $categoria ){

		if( ! is_string( $categoria ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*, b.*'  );
		$query->from( '#__virtuemart_categories_es_es AS a' );
		$query->innerJoin( '#__virtuemart_category_categories AS b ON b.category_child_id = a.virtuemart_category_id' );
		$query->where( 'a.category_name = '. $db->quote($categoria) );
		$query->where( 'b.category_parent_id = '. $db->quote($id) );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
	}

	public function setDateExpire( $id, $date ){


		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products'  );
		$query->set( 'fecha_expira = ' . $db->quote($date) );
		$query->where( 'virtuemart_product_id = '. $id );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}

	public function getAnuncioById( $id ){

		if( ! is_string( $id ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*, b.*' );
		$query->from( ' #__virtuemart_products AS a');
		$query->innerJoin( '#__virtuemart_products_es_es AS b ON a.virtuemart_product_id = b.virtuemart_product_id ' );

		$query->where( 'a.virtuemart_product_id  = '. $id);

		$db->setQuery( $query );

		return $db->loadObject();

	}

	public function getAnuncioBySku( $sku ){

		if( ! is_string( $sku ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*, b.*, c.*' );
		$query->from( ' #__virtuemart_products AS a');

		$query->innerJoin( '#__virtuemart_products_es_es AS b ON a.virtuemart_product_id = b.virtuemart_product_id ' );
		$query->innerJoin( '#__planes AS c ON c.id = b.plan ' );


		$query->where( 'a.product_sku  = '. $db->quote( $sku ));

		$db->setQuery( $query );

		return $db->loadObject();

	}
}