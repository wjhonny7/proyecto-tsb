 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="crear-anuncio">
<h2> EDITAR ANUNCIO </h2>

	<form id="anuncio-form" class="edit-anuncio-form" action="<?php echo JRoute::_(''); ?>" method="post">
		
		<table id="datosBasicos">
			<tbody>
				<tr>
				<td colspan="2">
					<h2 class="desc"> Datos básicos </h2>
				</td>
				</tr>				
				<tr>
				<td colspan="2">
					<label>Titulo de el anuncio</label>
					<input type ="text" name="titulo" value="<?= $this->anuncio->product_name ?>">
				</td>
				</tr>
				<tr>
				<td colspan="2">
					<label>Descripción corta</label>
					<input class="desc-corta" type ="text" name="descripcion_corta" value="<?= $this->anuncio->product_s_desc ?>">
				</td>
				</tr>
				<tr>
				<td colspan="2">
					<label>Descripción detallada</label>
					<textarea class="detallada"type ="text" name="descripcion_detallada" ><?= strip_tags($this->anuncio->product_desc) ?></textarea> 
				</td>
				</tr>
				<tr>
				<td class="corta">
					<label class="corta">Razón social</label>
					<input  class="corta"type ="text" name="razon_social" value="<?= $this->anuncio->razon_social ?>">
				</td>
				<td>
					<label class="corta">Categoría principal</label>
					<select name="categorias" id="categorias">
						<option>Seleccione</option>

						<?php 
						$categorias = AnunciosHelper::getCategories();

						foreach ($categorias as $key => $categoria) {
						?>
							<option value="<?= $categoria->virtuemart_category_id ?>"><?= $categoria->category_name ?></option>
						<?php
						}
						?>
					</select>
				</td>
				</tr>
				<tr>
				<td colspan="2">
					<label>Otras categorías</label>
					<input type="hidden" id="subcategorias" name="subcategoria" />
				</td>
				</tr>
				<tr>
				<td>
					<label class="corta">Precio</label>
					<input  class="corta"type ="text" name="precio" value="<?= Misc::deleteCeros($this->anuncio->product_price); ?>">
				</td>
				<td>
					<label class="corta">% Descuentos para miembros tsb</label>
					<select class="corta" id="descuentos" name="descuento">
						<option>10%</option>
						<option>20%</option>
						<option>30%</option>
					</select>
				</td>
				</tr>
			</tbody>
		</table>

		<div class="upload-images">
			<h2 class="desc">Imagenes</h2>
	        <div class="wrapper-images-uploader">
	            <div id="image-uploader">       
	                <noscript>          
	                    <p>Please enable JavaScript to use file uploader.</p>
	                    <!-- or put a simple form for upload here -->
	                </noscript>         
	            </div>
	            <p class="cursive">El tamaño máximo permitido para las imágenes es de 300 KB</p>
	            <div class="content-images content-multimedia">
	                <ul></ul>
	            </div>
	        </div>
	    </div>    

	    <div class="upload-ubicacion">

	    	<h2>Ubicación</h2>

		    <label for="direccion">Dirección<span class="star">*&nbsp;</span></label>

		    <input type="text" id="direccion" name="direccion" value="<?= $this->anuncio->direccion ?>"/>

		    <span class="title-map">Seleccione un punto en el mapa <br> o escriba la dirección del comercio</span>

		    <div id="map_canvas" style="width: 100%; height: 200px;"></div>

		</div>

		<?php 

			$user = AnunciosHelper::getUser();

			if ( $user->tipo_usuario == '2' ) {
		?>
				<table id="asignar">

					<tr>
						<td colspan="2">
							<h2 class="desc"> Asignacion de anuncio </h2>
						</td>
					</tr>
					<tr>
						<td>
							<label>Nombre  de usuario a quién asignar este anuncio</label>
		 				<select name="id_user" id="e2">

		 					<option>Usuarios</option>
		                	<?php 

		                	$all_users = AnunciosHelper::getUsersName();

		                	foreach ($all_users as $key => $user) {
		                	?>
								<option value="<?= $user->value ?>"><?= $user->text ?></option>
		                	<?php
		                	}

		                	 ?>
		                </select>
						</td>
					</tr>
				</table>

				<?php } ?>

				
		
		<div class="crear-complete">
			<input type="submit" id="edit-anuncio-button" value="Guardar Anuncio" />
			<input type="hidden" id="latlang" name="coordenadas" value="<?= $this->anuncio->coordenadas ?>">
			<input type="hidden" name="virtuemart_product_price_id" value="<?= $this->anuncio->virtuemart_product_price_id ?>">
			<input type="hidden" id="id" name="id" value="<?= $this->anuncio->virtuemart_product_id ?>">
			<input type="hidden" id="images" name="images" value='<?= json_encode($this->images); ?>'>
			<input type="hidden" id="categories" name="categorias" value='<?= json_encode($this->categorias); ?>'>

			
			
		</div>
	</form>

</div>