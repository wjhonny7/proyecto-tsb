 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
?>


<div class="contentedit">

	<div class="crear-view" id="system-message-container">
	<?php 

	if (isset($_REQUEST['transactionState'])) {
		
	

        if($_REQUEST['transactionState'] == 6 && $_REQUEST['polResponseCode'] == 5){
    ?>
            <div class="error">
                <h2>Transacción fallida</h2>
            </div>
    <?php  
        }else if($_REQUEST['transactionState'] == 6 && $_REQUEST['polResponseCode'] == 4){
    ?>
            <div class="error">
                <h2>Transacción rechazada</h2>
            </div>
    <?php
           
        }else if($_REQUEST['transactionState'] == 12 && $_REQUEST['polResponseCode'] == 9994){

    ?>
           <div class="warning">
                <h2>Pendiente, Por favor revisar si el débito fue realizado en el Banco</h2>
            </div>
    <?php
            
        }else if($_REQUEST['transactionState'] == 4 && $_REQUEST['polResponseCode'] == 1){
    ?>
            <div class="success">
                <h2>Transacción aprobada</h2>
            </div>
    <?php
        }else{
    ?>
            <div class="error">
                <h2><?php echo $_REQUEST['mensaje']; ?></h2>
            </div>
    <?php
            
        }
    }

    ?>
	</div>

	<div class="cuenta">
		<h2> Mi cuenta</h2>

		<ul>
			<li>
				<a href="index.php/anuncios"> <i class="nuevo"> Nuevo <br/> anuncio</i> </a> 
			</li>
			<li>
				<a href="#" id="desactivar-anuncios-button"><i class="desactivar"> Desactivar <br/> anuncio</i></a>
			</li>
			<li>
				<a href="#" id="activar-anuncios-button"><i class="activar"> Activar <br/> anuncio</i></a>
			</li>
		</ul>
	</div>

	<div class="tabs">
		<ul class="titles-tabs">
			<li>
				<a href="index.php/registrarse/profile?layout=edit"> <span class="perfil-off">Mi Perfil</span></a> 
			</li>
			<li>
				 <a href="index.php/anuncios?layout=listar"><span class="anuncios-on"> Mis Anuncios</span> </a> 
			</li>
		</ul>
	</div>

	<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
		<div class="lista-anuncios">

			<?php if ( count($this->anuncios) <= 0): ?>
				<h3>Actualmente no tiene anuncios activos.</h3>
				
			<?php endif ?>
			<?php if ( count($this->anuncios) > 0): ?>
				<table>
					<tr>
						<td>
							<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
							<label>Seleccionar todos los anuncios</label>
						</td>
					</tr>
				</table>
			<?php endif ?>
			<?php 
				$fecha = date('Y-m-d'); 
				foreach ($this->anuncios as $key => $anuncio) {
			?>
					<?php if ($anuncio->publicado == '1' && $fecha <= $anuncio->fecha_vencimiento): ?>
								<table border="0" class="a_active">
									<tbody>
										<tr>
											<td rowspan="3"><?php echo JHtml::_('grid.id', $key, $anuncio->virtuemart_product_id); ?><img width="172" height="126" src="images/stories/virtuemart/product/<?php echo $anuncio->file_title ?>"></td>
											<td> <h2 class="title"><a href="index.php/anuncios?task=anuncios.getAnuncio&id=<?php echo $anuncio->virtuemart_product_id ?>" ><?php echo $anuncio->product_name ?></a></h2> <span class="ubicacion"> <?php echo $anuncio->direccion ?></span></td>
											<td><button class="desactivar-button" data-id="<?php echo $anuncio->virtuemart_product_id ?>">Desactivar</button></td>
										</tr>
										<tr>
											<td colspan="2"><p class="description"><?php echo $anuncio->product_s_desc ?></p>
											</td>
										</tr>
										<tr>
											<td><p class="valor"> Valor <span class="precio"><?php echo '$ '. Misc::numberDots($anuncio->product_price) ?></span></p></td>
											<td> <a href="#" class="eliminar" data-id="<?php echo $anuncio->virtuemart_product_id ?>"> </a> </td>
										</tr>
									</tbody>
								</table>
					<?php endif; ?>

					<?php if ($anuncio->publicado == '0' && $fecha <= $anuncio->fecha_vencimiento): ?>
							<table border="0" class="a_inactivo">
								<tbody>
									<tr>
										<td rowspan="3"> <div class="image"><?php echo JHtml::_('grid.id', $key, $anuncio->virtuemart_product_id); ?><img width="172" height="126" src="images/stories/virtuemart/product/<?php echo $anuncio->file_title ?>"> <span class="aviso">Inactivo</span> </div> </td>
										<td> <h2 class="title"><a href="index.php/anuncios?task=anuncios.getAnuncio&id=<?php echo $anuncio->virtuemart_product_id ?>" ><?php echo $anuncio->product_name ?></a></h2> <span class="ubicacion"> <?php echo $anuncio->direccion ?></span></td>
										<td><button class="activar-button" data-id="<?php echo $anuncio->virtuemart_product_id ?>">Activar</button></td>
									</tr>
									<tr>
										<td colspan="2"><p class="description"><?php echo $anuncio->product_s_desc ?></p>
										</td>
									</tr>
									<tr>
										<td><p class="valor"> Valor <span class="precio"><?php echo '$ '. Misc::numberDots($anuncio->product_price) ?></span></p></td>
										<td> <a href="" class="eliminar" data-id="<?php echo $anuncio->virtuemart_product_id ?>"> </a> </td>
									</tr>
								</tbody>
							</table>
					<?php endif; ?>

					

					<?php if ( $fecha >= $anuncio->fecha_vencimiento ): ?>
							<table border="0" class="a_vencido">
								<tbody>
									<tr>
										<td rowspan="3"> <div class="image"><img width="172" height="126" src="images/stories/virtuemart/product/<?php echo $anuncio->file_title ?>">  <span class="aviso">Vencido</span> </div> </td>
										<td> <h2 class="title"> <?php echo $anuncio->product_name ?></h2> <span class="ubicacion"> <?php echo $anuncio->direccion ?></span></td>
										<td><a href="#" data-id="<?php echo $anuncio->virtuemart_product_id ?>" class="eliminar"> </a> </td>
									</tr>
									<tr>
										<td colspan="2"><p class="description"><?php echo $anuncio->product_s_desc ?></p>
										</td>
									</tr>
									<tr>
										<td><p class="valor"> Valor <span class="precio"> <?php echo '$ '. Misc::numberDots($anuncio->product_price) ?> </span></p></td>
										<td> </td>
									</tr>
									<tr>
										<td><p class="reactivar" >Reactivar anuncio </td>
										<td> 
										<select name="plan" id="list_planes<?php echo $anuncio->virtuemart_product_id ?>">
											<option value="0">Seleccione</option>
						                	<?php 

						                	$planes = AnunciosHelper::getplanes();

						                	foreach ($planes as $key => $plan) {
						                	?>
												<option value="<?= $plan->value ?>"><?= $plan->text ?></option>
						                	<?php
						                	}

						                	 ?>
					                	</select>
										</td>
										<td><button class="reactivar" data-id="<?php echo $anuncio->virtuemart_product_id ?>" >Reactivar</button> </td>
									</tr>
								</tbody>
							</table>		
					<?php endif ?>
						
			<?php
				}

			?>
			<div class="wrapper-paginator">
				<?php echo $this->upcomingPaginator; ?>
			</div>

			
		</div>
		
	</form>

	<div id="form-payu"></div>

</div>
<style type="text/css">
.optional{display: none;}
</style>

