<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class SiViewinforme extends JViewLegacy {


	protected $availaible_years;
	protected $availaible_themes;
	protected $availaible_tipos;
	
	// Function that initializes the view
	function display( $tpl = null ){
	
		// Add the toolbar with the actions
		$this->addToolbar();

		$model = new SiModelinforme();

		$this->availaible_years = $model->getYearsList();
		$this->availaible_tipos = $model->getTiposList();

	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::title( "Edici&oacute;n de Informe de Sistemas de Informaci&oacute;n" );
		JToolBarHelper::save( "informe.save" );
		JToolBarHelper::cancel( "cancel" );
	}

	/**
	 * Method to render the select option of available years
	 *
	 */
	
	function SelectYears( $year="" ){

		
		foreach ($this->availaible_years as $ano):
		
		$options[] = JHTML::_('select.option',$ano, $ano );
		
		endforeach;

		if(empty($year)){
			$year = date('Y');
		}
			
		echo JHTML::_('select.genericlist', $options,"ano", 'class="celuticket-input-selec-list selects-tipo-boleta "','value','text', $year);
		
	}

	/**
	 * Method to render the select option of available themes
	 *
	 * @param { string } the theme selected by default
	 */
	
	function SelectTipos( $tipo=NULL ){
	
	
		foreach ( $this->availaible_tipos as $_tipo ):
	
		$options[] = JHTML::_( 'select.option', $_tipo->id , $_tipo->nombre );
	
	
		endforeach;
	
			
		echo JHTML::_('select.genericlist', $options,"tipo", 'class=""','value','text', $tipo );
	
	
	}


}
?>