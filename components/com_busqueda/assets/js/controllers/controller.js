/**
*
* Controller for { Object }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var ObjectController = function( a ){
		
		// atributes, selector or global vars
		this.attribute = 'my-atribute';
		
	};
	
	ObjectController.prototype ={
			

			/**
			 * Function header when calls the model method and gives the view the response
			 *
			 * @param {}
			 * @return {}
			 */
			myFunctionAction: function( data ){
				
				var success = function( data ){

					view.onCompleteAction( data );
				};

				return model.myMethod( sucess, data );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.ObjectController = new ObjectController();
	
})( jQuery, this, this.document, this.ObjectModel, this.ObjectView, this.Misc, undefined );