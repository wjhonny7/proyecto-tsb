<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/misc.php' );

$controller = JControllerLegacy::getInstance('busqueda');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>