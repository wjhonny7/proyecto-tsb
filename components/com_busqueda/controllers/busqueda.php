<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class BusquedaControllerBusqueda extends JController{

	protected $count;

	protected $limit = 5;

	protected $query;

	public function search(){

		$input = JFactory::getApplication()->input;
		$app = JFactory::getApplication();



		// Get the post from data
		$data = $input->getArray( $_POST );

		// // Combine data and user state filters to preserve the filters
		if( empty( $data ) )
			$data = unserialize( $app->getUserState( 'search' ) );

		// $data = array_merge( $_data, $data );

		// Save the filters into application
		$app->setUserState( 'search', serialize($data) );

		// Get paginator params
		$offset = JRequest::getVar( 'offset' );
		$page = JRequest::getVar( 'page' );

		$paginator = array();
		$paginator['limitstart'] = isset( $offset ) ? $offset : 0;
		$paginator[ 'limit' ] = $this->limit;

		// //Get query base
		$db = JFactory::getDbo();
		$this->query = $db->getQuery( true );

		// Get by code
		$this->query->select( 'producto.*, _producto.product_sku, medias.file_url as image, productprices.product_price as price, categorias.*' );

		$this->query->from( $db->quoteName( 'jos_virtuemart_products_es_es' ) . ' as producto' );

		$this->query->innerJoin( $db->quoteName( 'jos_virtuemart_products' ) . ' as _producto ON producto.virtuemart_product_id = _producto.virtuemart_product_id' );
		$this->query->where( '_producto.published = 1' );

		//Select media
		$this->query->innerJoin( 'jos_virtuemart_product_medias AS productmedias ON producto.virtuemart_product_id = productmedias.virtuemart_product_id' );
		$this->query->group( 'productmedias.virtuemart_product_id' );

		// Select images
		$this->query->innerJoin( 'jos_virtuemart_medias AS medias ON productmedias.virtuemart_media_id = medias.virtuemart_media_id' );
		
		// Select prices
		$this->query->innerJoin( 'jos_virtuemart_product_prices AS productprices ON producto.virtuemart_product_id = productprices.virtuemart_product_id' );

		$this->query->innerJoin( 'jos_virtuemart_product_categories AS categorias ON categorias.virtuemart_product_id = producto.virtuemart_product_id' );

		

		if( ! empty( $data['clave'] ) ){
			$this->query->where( 'producto.product_name LIKE "%' . $data['clave'] . '%"');
		}

		if( ! empty( $data['ciudad'] ) ){
			$this->query->where( 'producto.direccion LIKE "%' . $data['ciudad'] . '%"');
		}

		if( ! empty( $data['marca'] ) ){
			$this->query->where( 'producto.product_desc LIKE "%' . $data['marca'] . '%"');
		}


		if( ! empty( $data['categorias'] ) ){

			$model = $this->getModel('busqueda');

			$categorias = $model->getSubcategorias( $data['categorias'] );

			$select = '(';

			foreach ($categorias as $key => $categoria) {
				$childs = $model->getSubcategorias( $categoria->category_child_id );

				foreach ($childs as $key => $child) {

					$select .= 'categorias.virtuemart_category_id ='. $db->quote($child->category_child_id).' OR ';

				}
				
			}

			$select = substr($select, 0, -3);

			$select .= ')';

			$this->query->where( $select );
		}

		
		// Set the query and get all objects
		//var_dump( $this->query->__toString() );
		$db->setQuery( $this->query );
		$this->count = count( $db->loadObjectList() );
		$db->setQuery( $this->query, $paginator['limitstart'], $paginator[ 'limit' ] );
		$ads = $db->loadObjectList();

		

		// // Get the view
		$view = $this->getView('producto', 'html');

		$pages = $this->setPaginator();
		// // Assign to the view attrs
		$view->assignRef( 'items', $ads );
		$view->assignRef( 'pages', $pages );
		$view->assignRef( 'count', $this->count );
		
		if( is_numeric( $page ) )
			$view->assignRef( 'active', $page );

		$view->assignRef( 'limit', $this->limit );
		$view->display();



	}

	/**
	* Sets the paginator pages
	*
	*/
	protected function setPaginator(){

		$pages = ceil( $this->count / $this->limit );

		return $pages;
	}
	
}
?>