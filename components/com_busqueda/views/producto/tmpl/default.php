<?php

/**
* Default Template for object
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();

$document->addStyleSheet( $url.'components/com_virtuemart/assets/css/vmsite-ltr.css' );

?>
<h1>Resultados de la búsqueda</h1>
<div class="browse-view">
	<?php
	if( count($this->items) <= 0 ){
	?>
		<h3>No hay resultados para la búsqueda.</h3>
	<?php
	}

	foreach ($this->items as $key => $value) {
	?>
	<div class="row" style="display: inline-block;">
		<div class="product floatleft width100 ">
			<div class="spacer">
				<div class="width30 floatleft center">
				    <div class="MagicToolboxContainer" style="max-width: 506px">     
				    	
					    <img itemprop="image" src="<?= $url.$value->image ?>" alt="Ideal accesorios" style="opacity: 1;">
					    <br>                   
					    <div></div>     
					</div> 
				</div>

				<div class="width70 floatright">

					<h2><a href="index.php/cali/unicentro/<?= $value->slug ?>-detail"><?= $value->product_name ?></a></h2>

					<p class="product-operation">
						<?= $value->product_sku ?>					
					</p>

					<p class="product_s_desc">
							<?= strip_tags($value->product_s_desc) ?>						
					</p>
					
					<div class="product-price marginbottom12" id="productPrice7">
						<div class="PricevariantModification" style="display : none;">Modificador de variación de precio: 
							<span class="PricevariantModification"></span>
						</div>
						<div class="price-crossed"></div>
						<div class="PricesalesPrice" style="display : block;">Valor: 
							<span class="PricesalesPrice">$ <?= Helper::numberDots($value->price); ?></span>
						</div>
						<div class="PricediscountAmount" style="display : none;">Descuento: 
							<span class="PricediscountAmount"></span>
						</div>
						<div class="PricetaxAmount" style="display : none;">
							Cantidad de impuestos: 
							<span class="PricetaxAmount"></span>
						</div>
					</div>

					
						
					<p class="regresar">
						
						<a href="index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=<?= $value->virtuemart_product_id ?>" title="Ideal accesorios" class="product-details">Ver Detalle</a>	
						
										
					</p>

				</div>
				<div class="clear"></div>
			</div>
			<!-- end of spacer -->
		</div>

	</div>


		
	<?php
	}
	?>
	<div class="vm-pagination">
    <?php
        // Get the paginator
        $this->getPaginator();
        echo $this->paginator;
    ?>
    </div>
</div>