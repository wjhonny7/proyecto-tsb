<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class BusquedaViewProducto extends JViewLegacy {

	protected $items;
	public $paginator;
	public $option;
	public $pages = 1;
	public $active = 1;
	public $limit = 8;
	public $count = 1;
	
	// Function that initializes the view
	function display( $tpl = null ){
	
		parent::display( $tpl );
	
	}
	
	/**
	* Crear paginador
	*
	*/
	/**
	* Prints user paginator
	*
	*/
	protected function getPaginator( $task = 'busqueda.search' ){

		$url = JFactory::getURI();
		$url = $url->root();

		if( $this->pages > 1 ){

			$next = $this->active + 1;
			$next = ( $next > $this->pages ) ? $this->pages : $next;

			$offsetNext = ( $next == 1 ) ? 0 : ( $next - 1 ) * $this->limit;

			$prev = $this->active - 1;
			$prev = ( $prev < 1 ) ? 1 : $prev;

			$offsetPrev = ( $prev == 1 ) ? 0 : ( $prev - 1 ) * $this->limit;
			
			$paginator .= '<ul>
			<li class="pagination-start">
				<a href="index.php?option=com_busqueda&task='. $task .'&offset=' . $offsetPrev . '&page=' . $prev . '" >
				<span class="pagenav">< Inicio</span>
				</a>
			</li>';

			for ( $i = 1; $i <= $this->pages; $i++ ):

				$offset = ( $i == 1 ) ? 0 : ( $i - 1 ) * $this->limit;
				$class = ( $i == $this->active ) ? 'class="active"' : '';

				$paginator .= '<li><a ' . $class  . ' href="index.php?option=com_busqueda&task='. $task .'&offset=' . $offset . '&page=' . $i . '">' . $i . '</a></li>';
			endfor;

			$paginator .= '<li class="pagination-end">
			<a class="pagenav" href="index.php?option=com_busqueda&task='. $task .'&offset=' . $offsetNext . '&page=' . $next . '" >
				<span>Último ></span>
			</a></li></ul>';
		}

		$this->paginator = $paginator;
	}
	


}
?>