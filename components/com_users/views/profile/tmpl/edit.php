<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
//load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load( 'plg_user_profile', JPATH_ADMINISTRATOR );


$user	= JFactory::getUser();
$userId	= (int) $user->get('id');
$model	= $this->getModel('Profile', 'UsersModel');

$userModel = $model->getUser($userId);


if ($userModel->block == '1') {
	$app = JFactory::getApplication();
	$app->logout();
	$app->redirect('index.php');
}
?>

<div class="contentedit">
	<div class="cuenta">
		<h2> Mi cuenta</h2>

		<ul>
			<li>
				 <a href="index.php/anuncios"> <i class="nuevo"> Nuevo <br/> anuncio</i> </a> 
			</li>
			<li>
				<!-- <a href="#"> --><i class="desactivar"> Desactivar <br/> anuncio</i><!-- </a> -->
			</li>
			<li>
				<!-- <a href="#"> --><i class="activar"> Activar <br/> anuncio</i><!-- </a> -->
			</li>
		</ul>
	</div>

	<div class="tabs">
		<ul class="titles-tabs">
			<li>
				<a href="index.php/registrarse/profile?layout=edit"> <span class="perfil">Mi Perfil</span></a> 
			</li>
			<li>
				 <a href="index.php/anuncios?layout=listar"><span class="anuncios"> Mis Anuncios</span> </a> 
			</li>
		</ul>
	</div>
	<div class="profile-edit<?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	<?php endif; ?>
	<h2>Datos Basicos</h2>
	<form id="member-profile" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
	<?php foreach ($this->form->getFieldsets() as $group => $fieldset):// Iterate through the form fieldsets and display each one.?>
		<?php $fields = $this->form->getFieldset($group);?>
		<?php if (count($fields)):?>

			<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.?>
		<!-- 	<legend><?php echo JText::_($fieldset->label); ?></legend> -->
			<?php endif;?>
		<table class="edit-form">
			
			
			<?php foreach ($fields as $field):// Iterate through the fields in the set and display them.?>
				<?php if ($field->hidden):// If the field is hidden, just display the input.?>
					<?php echo $field->input;?>
				<?php else:?>
				<tr>
					<td>
						<?php echo $field->label; ?>
						<?php if (!$field->required && $field->type!='Spacer' && $field->name!='jform[username]'): ?>
							<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
						<?php endif; ?>

						<?php echo $field->input; ?>
					</td>
				</tr>	
					
				<?php endif;?>
			<?php endforeach;?>
		
		</table>
		<?php endif;?>
	<?php endforeach;?>

			<div>
				<button type="submit" class="validate"><span><?php echo JText::_('Actualizar'); ?></span></button>
				<!--<?php //echo JText::_('COM_USERS_OR'); ?> -->
				<button ><a href="<?php echo JRoute::_(''); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a></button>

				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="profile.save" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</form>
	</div>
</div>
<style type="text/css">
.optional{display: none;}
</style>