(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 395,
	height: 430,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"images/sombra.png", id:"sombra"}
	]
};

// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_239 = function() {
		this.gotoAndPlay(85)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(239).call(this.frame_239).wait(1));

	// slogan
	this.instance = new lib.slogan();
	this.instance.setTransform(195,417.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({alpha:1},10).wait(171));

	// proyecto
	this.instance_1 = new lib.proyecto();
	this.instance_1.setTransform(196.4,385,1,1,0,0,0,-0.2,0);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(49).to({_off:false},0).to({alpha:1},10).wait(181));

	// avion
	this.instance_2 = new lib.avion();
	this.instance_2.setTransform(119.1,253,1,1,0,0,0,-0.7,1.3);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(49).to({_off:false},0).to({x:242,y:198.3,alpha:1},30,cjs.Ease.get(1)).wait(161));

	// mundo
	this.instance_3 = new lib.mundo();
	this.instance_3.setTransform(197.8,171.5,0.063,0.063);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:720},20,cjs.Ease.get(1)).wait(216));

	// notas
	this.instance_4 = new lib.notas();
	this.instance_4.setTransform(79,171.7,0.122,0.122);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({_off:false},0).to({scaleX:1.11,scaleY:1.11,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(105).to({scaleX:1.11,scaleY:1.11},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(86));

	// globo
	this.instance_5 = new lib.globo();
	this.instance_5.setTransform(144.6,272.2);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(24).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(110).to({scaleX:1.2,scaleY:1.2},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(76));

	// barras
	this.instance_6 = new lib.barras();
	this.instance_6.setTransform(273.2,259.7);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(29).to({_off:false},0).to({scaleX:1.11,scaleY:1.11,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(110).to({scaleX:1.11,scaleY:1.11},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(71));

	// nube
	this.instance_7 = new lib.nube();
	this.instance_7.setTransform(307.6,171.7);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(34).to({_off:false},0).to({scaleX:1.36,scaleY:1.36,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(110).to({scaleX:1.36,scaleY:1.36},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(66));

	// maleta
	this.instance_8 = new lib.maleta();
	this.instance_8.setTransform(234.9,70.3);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(39).to({_off:false},0).to({scaleX:1.22,scaleY:1.22,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(110).to({scaleX:1.22,scaleY:1.22},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(61));

	// gente
	this.instance_9 = new lib.gente();
	this.instance_9.setTransform(125.5,86.3);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(44).to({_off:false},0).to({scaleX:1.34,scaleY:1.34,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5).wait(110).to({scaleX:1.34,scaleY:1.34},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5).wait(56));

	// punteada
	this.instance_10 = new lib.punteada();
	this.instance_10.setTransform(193.9,172.9);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(9).to({_off:false},0).to({rotation:720,alpha:1},15,cjs.Ease.get(1)).wait(216));

	// fondo
	this.instance_11 = new lib.fondo();
	this.instance_11.setTransform(194.2,180.9);
	this.instance_11.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({alpha:1},14).wait(226));

	// sombra
	this.instance_12 = new lib.sombra_1();
	this.instance_12.setTransform(194.1,211.8);
	this.instance_12.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({alpha:1},14).wait(226));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(218.6,227,346,372.8);


// symbols:
(lib.sombra = function() {
	this.initialize(img.sombra);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,346,346);


(lib.sombra_1 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.sombra();
	this.instance.setTransform(-173,-173);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-173,-173,346,346);


(lib.slogan = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#444343").s().p("AgoAvQgHgLAEgUQAEgSAMgMQANgOAPAAQAKAAAFAEQADACAEAGIAJgqIAOAAIgZByIgNAAIADgMQgGAIgGAEQgJADgIAAQgNAAgJgMgAgQgHQgJAHgDAPQgDAOAEAIQAEAKANAAQAJAAAHgJQAHgIAEgQQADgOgEgIQgFgIgKAAQgIAAgJAJg");
	this.shape.setTransform(184.7,-1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#444343").s().p("AghAhQgJgMAFgVQAEgSAOgNQANgMAQAAQAKAAAHAEQAIAFACAGQADAHAAAIQAAAGgDAKIg8AAQgCAOAEAHQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACIgKABQgPAAgJgLgAgJgYQgJAIgCAKIAtAAQABgKgCgFQgDgKgOAAQgIAAgIAHg");
	this.shape_1.setTransform(175.2,-0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#444343").s().p("AAVA6IgBgFIAFgcQACgKgHgGQgGgCgIAAIgiAAIgKAzIgQAAIAZhyIAzAAQAOgBAHAEQAPAIgEATQgCALgGAGQgFAHgJACQAGADACAEQADAGgCAJIgDAPIgBALQAAAFAEABIgBAEgAgZgDIAiAAQALAAAIgFQAHgEADgMQACgMgIgEQgDgDgJAAIglAAg");
	this.shape_2.setTransform(165.2,-1.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#444343").s().p("AANArIALg0QACgHgBgFQgDgIgJAAIgJABQgFACgEAFQgFAFgCAEQgCAEgCAIIgJArIgOAAIAShTIANAAIgCAMQAJgIAEgDQAHgDAIAAQASAAAEANQACAGgDANIgLA1g");
	this.shape_3.setTransform(149.9,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#444343").s().p("AghAhQgJgMAFgVQAEgSAOgNQANgMAQAAQAKAAAHAEQAHAEADAHQADAHAAAIQAAAIgDAIIg8AAQgCANAEAIQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACQgDABgHAAQgQAAgIgLgAgJgYQgIAHgDALIAtAAQABgIgCgHQgDgKgOAAQgIAAgIAHg");
	this.shape_4.setTransform(141.1,-0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#444343").s().p("AggAkQgGgJACgMIAOAAQgBAHACAEQAEAGANABQAHAAAGgEQAHgDABgIQACgFgFgDIgLgEIgJgDQgJgCgFgBQgJgHADgJQADgNAKgHQAKgHAMgBQATAAAHALQADAIgBAIIgOAAQABgEgCgFQgEgFgMgBQgGAAgFAEQgFADgBAFQgBAGAFADQACABAFACIAIACQAOAEAFACQAHAGgCALQgCALgLAIQgKAJgQgBQgTAAgGgIg");
	this.shape_5.setTransform(128,-0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#444343").s().p("AgjAhQgIgNAFgTQAEgTAOgNQANgNARAAQAQAAAJALQAJALgFAVQgEATgNANQgMANgTAAQgSABgIgMgAgNgWQgIAMgDAKQgCAPAEAIQAEAJAMAAQANAAAHgLQAIgKACgLQADgMgCgHQgEgNgOAAQgLABgJAJg");
	this.shape_6.setTransform(119.5,-0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#444343").s().p("AgGA2QgKAAgCgFQgDgFACgIIAMg2IgMAAIADgMIALAAIAFgXIANAAIgFAXIANAAIgCAMIgOAAIgJA1QgCAFADABIAEABIACAAIADAAIgDALIgDABg");
	this.shape_7.setTransform(113.4,-1.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#444343").s().p("AggAgQgIgMAEgSQAGgVANgMQANgMAPAAQAPAAAHAGQAHAHgCARIgOAAQABgHgEgGQgDgFgKAAQgLAAgJAOQgGAJgCAKQgDANADAIQAEAJALAAQAIAAAGgGQAGgFAEgJIAOAAQgHARgKAHQgMAIgMAAQgQAAgIgMg");
	this.shape_8.setTransform(106.6,-0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#444343").s().p("AghAhQgJgMAFgVQAEgSAOgNQANgMAQAAQAKAAAHAEQAHAEADAHQADAHAAAIQAAAIgDAIIg8AAQgCANAEAIQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACIgKABQgQAAgIgLgAgJgYQgIAHgDALIAtAAQABgIgCgHQgDgKgOAAQgIAAgIAHg");
	this.shape_9.setTransform(97.9,-0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#444343").s().p("AguA6IADgNIAIACQAEAAACgCIAEgDIAJgPIgMhVIAQAAIAIBDIAjhDIAQAAIgqBHQgRAggHAHQgHAHgMAAg");
	this.shape_10.setTransform(89.5,1.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#444343").s().p("AgjAhQgIgNAFgTQAEgTAOgNQANgNARAAQAQAAAJALQAJALgFAVQgEATgNANQgMANgUAAQgRABgIgMgAgNgWQgIAMgDAKQgCAPAEAIQAEAJAMAAQANAAAHgLQAIgKACgLQADgMgCgHQgEgNgPAAQgKABgJAJg");
	this.shape_11.setTransform(80.8,-0.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#444343").s().p("AgcArIAShTIALAAIgBAPQACgGAHgFQAHgGAJAAIAEAAIgDAPIgEAAQgMAAgGAHQgGAHgCAJIgKAvg");
	this.shape_12.setTransform(74.2,-0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#444343").s().p("AgzA6IAYhyIAyAAQAQAAAIAIQAHAJgDAPQgDAOgLAKQgKAIgSAAIgiAAIgLAygAgWgDIAdAAQAKAAAIgFQAHgEADgMQADgMgJgEQgEgDgKAAIgdAAg");
	this.shape_13.setTransform(66,-1.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#444343").s().p("AghAhQgJgMAFgVQAEgSAOgNQANgMAQAAQAKAAAHAEQAIAEACAHQADAHAAAIQAAAFgDALIg8AAQgCAOAEAHQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACIgKABQgPAAgJgLgAgJgYQgJAIgCAKIAtAAQABgKgCgFQgDgKgOAAQgIAAgIAHg");
	this.shape_14.setTransform(51.2,-0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#444343").s().p("AgoAvQgIgLAFgUQAEgRAMgNQAMgOAQAAQAKAAAFAEQADACAEAGIAJgqIAOAAIgZByIgNAAIACgMQgGAIgGAEQgIADgIAAQgOAAgIgMgAgRgHQgIAHgDAPQgDANAEAJQADAKANAAQAKAAAGgJQAIgHAEgRQADgOgFgIQgFgIgJAAQgIAAgKAJg");
	this.shape_15.setTransform(42.7,-1.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#444343").s().p("AgjAlQgFgHACgLQACgLAJgHQAIgEANgCIAVgDQAGgBACgDIACgFQABgIgEgDQgFgDgJAAQgJAAgGAGQgDADgDAHIgNAAQADgQAMgGQALgGALAAQAPAAAIAGQAIAFgDAMIgKAvQgBABAAAAQAAABAAAAQAAABAAAAQAAAAABABIADABIADAAIACgBIgCALIgFABIgFAAQgIAAgCgFIAAgIQgFAFgKAFQgGAEgLAAQgMAAgGgHgAANABIgIACIgHABQgIABgEACQgIAEgBAIQgCAGAEAEQAEADAGAAQAIAAAFgDQANgGADgNIADgKQgEABgEAAg");
	this.shape_16.setTransform(28.5,-0.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#444343").s().p("AgoAvQgIgMAFgTQADgQANgOQANgOAPAAQAJAAAGAEQAEADADAFIAJgqIAOAAIgZByIgNAAIADgMQgIAJgFADQgIADgIAAQgOAAgIgMgAgQgHQgIAHgEAPQgDANAEAJQADAKANAAQAKAAAGgJQAJgIADgQQAEgOgGgIQgEgIgKAAQgJAAgIAJg");
	this.shape_17.setTransform(20.2,-1.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#444343").s().p("AgSA6IAShTIAMAAIgPBTgAABgoIAEgQIAOAAIgDAQg");
	this.shape_18.setTransform(13.8,-1.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#444343").s().p("AgZAqIgMhTIAQAAIAIBDIAkhDIAQAAIgwBTg");
	this.shape_19.setTransform(8.5,-0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#444343").s().p("AgjAlQgFgHACgLQACgLAJgHQAIgEANgCIAVgDQAFgBADgDIACgFQABgIgEgDQgFgDgJAAQgKAAgGAGQgEAFgBAFIgNAAQADgQAMgGQAKgGALAAQAQAAAHAGQAJAFgDAMIgKAvQAAABgBAAQAAABAAAAQAAABAAAAQABAAAAABIADABIACAAIADgBIgCALIgFABIgFAAQgIAAgCgFIgBgIQgEAFgKAFQgGAEgLAAQgMAAgGgHgAANABIgIACIgHABQgJABgDACQgIAEgBAIQgCAGAEAEQADADAHAAQAIAAAFgDQANgGADgNIACgKIgHABg");
	this.shape_20.setTransform(-5.7,-0.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#444343").s().p("AANArIALg0QACgHgBgFQgDgIgJAAIgJABQgFACgFAFQgEAEgCAFIgEAMIgJArIgPAAIAShTIAOAAIgDAMQAJgIAEgDQAIgDAIAAQASAAAEANQACAIgDALIgMA1g");
	this.shape_21.setTransform(-14.6,-0.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#444343").s().p("AgmAfQgCgHACgLIAMg3IAPAAIgMA2QgCAGACAFQABAHALAAQANABAIgNQAEgHADgLIAJgqIAOAAIgSBTIgNAAIADgMQgDADgGAFQgHAGgMAAQgSAAgEgMg");
	this.shape_22.setTransform(-23.2,0);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#444343").s().p("AguA6IADgNIAIACQAEAAACgCIAEgDIAJgPIgNhVIARAAIAIBDIAjhDIAQAAIgqBHQgQAfgIAIQgGAHgMAAg");
	this.shape_23.setTransform(-36.4,1.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#444343").s().p("AgjAlQgFgHACgLQACgMAJgGQAIgEANgCIAVgDQAFgBACgDIADgFQABgIgEgDQgFgDgKAAQgJAAgGAGQgDAEgCAGIgNAAQADgQALgGQALgGALAAQAQAAAHAGQAJAFgDAMIgKAvQAAABAAAAQgBABAAAAQAAABABAAQAAAAAAABIADABIACAAIADgBIgCALIgGABIgEAAQgIAAgCgFIgBgIQgEAFgKAFQgGAEgLAAQgMAAgGgHgAANABIgIACIgHABIgMADQgHAEgDAIQgBAGAEAEQADADAHAAQAIAAAFgDQAOgGACgNIACgKg");
	this.shape_24.setTransform(-49.7,-0.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#444343").s().p("AgoAvQgIgLAEgUQAFgSAMgMQAMgOAQAAQAKAAAFAEQADACAEAGIAJgqIAOAAIgZByIgNAAIACgMQgGAIgGAEQgIADgIAAQgOAAgIgMgAgRgHQgIAHgDAPQgDANAEAJQADAKANAAQAKAAAGgJQAIgHAEgRQADgOgFgIQgFgIgJAAQgIAAgKAJg");
	this.shape_25.setTransform(-58,-1.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#444343").s().p("AgSA6IAShTIAMAAIgQBTgAABgoIAEgQIAOAAIgDAQg");
	this.shape_26.setTransform(-64.4,-1.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#444343").s().p("AgYAqIgOhTIARAAIAIBDIAkhDIAQAAIgwBTg");
	this.shape_27.setTransform(-69.7,-0.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#444343").s().p("AghAhQgJgMAFgVQADgRAPgOQANgMAQAAQAKAAAHAEQAHAEADAHQADAHAAAIQAAAIgDAIIg8AAQgCANAEAIQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACQgDABgHAAQgQAAgIgLgAgJgYQgIAHgDALIAtAAQABgIgCgHQgDgKgOAAQgIAAgIAHg");
	this.shape_28.setTransform(-83.7,-0.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#444343").s().p("AgoAvQgHgLAEgUQADgRANgNQANgOAPAAQAKAAAFAEQADACAEAGIAJgqIAOAAIgZByIgNAAIADgMQgIAIgEAEQgJADgIAAQgNAAgJgMgAgQgHQgJAHgDAPQgDAOAEAIQAEAKAMAAQAKAAAHgJQAHgIAEgQQADgOgEgIQgFgIgKAAQgJAAgIAJg");
	this.shape_29.setTransform(-92.2,-1.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#444343").s().p("AgjAhQgIgNAFgTQAEgTAOgNQANgNAQAAQARAAAJALQAJALgFAVQgEATgNANQgMANgUAAQgRABgIgMgAgNgWQgIAMgDAKQgDANAEAKQAFAJAMAAQANAAAHgLQAHgJADgMQADgMgCgHQgEgNgPAAQgKABgJAJg");
	this.shape_30.setTransform(-106.2,-0.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#444343").s().p("AgGA2QgKAAgCgFQgDgEACgJIAMg2IgMAAIADgMIALAAIAFgXIANAAIgFAXIANAAIgCAMIgOAAIgKA1QgBAFADABIAEABIACAAIADAAIgDALIgDABg");
	this.shape_31.setTransform(-112.2,-1.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#444343").s().p("AggAgQgIgMAEgSQAFgTANgOQAOgMAPAAQAOAAAHAGQAIAIgCAQIgOAAQABgHgEgGQgDgFgKAAQgLAAgJAOQgGAJgDAKQgCANADAIQAEAJALAAQAIAAAFgGQAHgFAEgJIAOAAQgGAQgLAIQgMAIgMAAQgQAAgIgMg");
	this.shape_32.setTransform(-119,-0.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#444343").s().p("AghAhQgJgMAFgVQAEgSAOgNQANgMAQAAQAKAAAHAEQAHAEADAHQADAHAAAIIgDAQIg8AAQgCANAEAIQAEAIAMAAQAJAAAIgIQAEgDAEgGIAOAAQgCAFgEAFQgEAFgFAEQgIAHgKACQgDABgHAAQgQAAgIgLgAgJgYQgIAHgDALIAtAAQABgIgCgHQgDgKgOAAQgIAAgIAHg");
	this.shape_33.setTransform(-127.7,-0.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#444343").s().p("AguA6IADgNIAIACQAEAAACgCIAEgDIAJgPIgNhVIARAAIAIBDIAjhDIAQAAIgqBHQgRAggHAHQgHAHgMAAg");
	this.shape_34.setTransform(-136.2,1.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#444343").s().p("AgjAhQgIgNAFgTQAEgTAOgNQANgNAQAAQARAAAJALQAJALgFAVQgEATgNANQgMANgUAAQgRABgIgMgAgNgWQgIAKgDAMQgDANAEAKQAFAJAMAAQAMAAAIgLQAHgIADgNQADgNgCgGQgEgNgPAAQgKABgJAJg");
	this.shape_35.setTransform(-144.8,-0.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#444343").s().p("AgcArIAShTIALAAIgBAPQABgFAIgGQAHgGAJAAIAEAAIgDAPIgEAAQgMAAgGAHQgGAHgCAJIgKAvg");
	this.shape_36.setTransform(-151.5,-0.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#444343").s().p("AgzA6IAZhyIAyAAQAQAAAHAIQAHAJgDAPQgDAOgKAKQgLAIgRAAIgjAAIgKAygAgWgDIAdAAQALAAAHgFQAIgEACgMQADgMgIgEQgFgDgJAAIgeAAg");
	this.shape_37.setTransform(-159.7,-1.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#444343").s().p("AANArIALg0QACgIgCgEQgBgIgLAAIgIABQgFACgFAFQgFAFgBAEIgEAMIgKArIgOAAIAShTIAOAAIgDAMQAJgIAEgDQAIgDAIAAQASAAADANQADAHgDAMIgMA1g");
	this.shape_38.setTransform(-174.6,-0.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#444343").s().p("AgzAmQgDgLAEgWIAOg/IAQAAIgPBFQgDANADAJQAEAMARAAQASAAAKgOQAFgGADgOIAQhFIAPAAIgNA/QgFAVgIAMQgPAVgcAAQgdAAgGgVg");
	this.shape_39.setTransform(-183.9,-1.5);

	this.addChild(this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-189.3,-7.5,378.6,15.1);


(lib.nube = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9F8F7").s().p("AASBjIAAgfIA7AAIhNhJIhKBJIA0AAIAAAfIg+AAQgcAAgTgVQgTgVAAgeQAAgcATgVQATgVAcAAIADAAQAMgZAWgPQAWgPAZAAQAaAAAWAPQAWAPAMAZIADAAQAcAAATAVQATAVAAAcQAAAegTAVQgTAVgcAAg");
	this.shape.setTransform(1.2,-2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F69531").s().p("AiVCVQg+g+AAhXQAAhWA+g/QA+g+BXAAQBXAAA/A+QA+A/AABWQAABXg+A+Qg/A/hXAAQhXAAg+g/g");
	this.shape_1.setTransform(0.1,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D2D3D4").s().p("AitCuQhIhJAAhlQAAhkBIhJQBJhIBkAAQBmAABIBIQBIBJAABkQAABlhIBJQhIBIhmAAQhkAAhJhIg");

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,49.3,49.3);


(lib.notas = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FBFAFC").s().p("Ah0DCQgTAAgOgOQgOgOAAgVIAMAAQAPAAAKgKQAKgLAAgPQAAgPgKgLQgKgKgPgBIgMAAIAAiQIAMAAQAPAAAKgKQAKgKAAgQQAAgPgKgLQgKgLgPABIgMAAQAAgVAOgOQAOgOATAAIEAAAQAUAAAOAOQANAOAAAVIAAEhQAAAVgNAOQgOAOgUAAgAhFCqIDRAAQAKAAAHgHQAHgIAAgKIAAkhQAAgKgHgIQgHgHgKAAIjRAAgAgiB5QgFAAgDgEQgDgDAAgFQAAgFADgEQADgDAFAAIBcAAQAFAAADADQAEAEAAAFQAAAFgEADQgDAEgFAAgAiuB5QgFAAgEgEQgDgDAAgFQAAgFADgEQAEgDAFAAIAXAAQAFAAADADQAEAEAAAFQAAAFgEADQgDAEgFAAgAgiBIQgFABgDgEQgDgEAAgEQAAgFADgEQADgEAFAAICjAAQAFAAADAEQADAEAAAFQAAAEgDAEQgDAEgFgBgAgiAYQgFAAgDgEQgDgDAAgGQAAgFADgDQADgDAFAAICjAAQAFAAADADQADADAAAFQAAAGgDADQgDAEgFAAgAgigXQgFAAgDgDQgDgEAAgFQAAgFADgEQADgDAFAAICjAAQAFAAADADQADAEAAAFQAAAFgDAEQgDADgFAAgAgihIQgFAAgDgDQgDgDAAgFQAAgGADgDQADgEAFAAICjAAQAFAAADAEQADADAAAGQAAAFgDADQgDADgFAAgAiuhgQgFABgEgEQgDgEAAgFQAAgFADgEQAEgDAFAAIAXAAQAFAAADADQAEAEAAAFQAAAGgEADQgDAEgFgBgAgih4QgFAAgDgEQgDgDAAgFQAAgFADgEQADgDAFAAICjAAQAFAAADADQADAEAAAFQAAAFgDADQgDAEgFAAg");
	this.shape.setTransform(-0.9,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1292E").s().p("Aj8D8QhohoAAiUQAAiTBohpQBphoCTAAQCTAABqBoQBoBpAACTQAACUhoBoQhqBpiTAAQiTAAhphpg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D2D3D4").s().p("AkkElQh6h5AAisQAAiqB6h6QB6h6CqAAQCsAAB5B6QB6B6AACqQAACsh6B5Qh5B6isAAQiqAAh6h6g");

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-41.5,-41.5,83.1,83.1);


(lib.maleta = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiyBaIAAhyIBcAAIAAhBICtAAIAABBIBcAAIAABygAg3gYIBvAAIAAgkIhvAAg");
	this.shape.setTransform(-0.4,-8.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghANIAAgZIBDAAIAAAZg");
	this.shape_1.setTransform(-0.4,4.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AiyAtIAAhZIB9AAIAAAtIBrAAIAAgtIB9AAIAABZg");
	this.shape_2.setTransform(-0.4,7.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#789647").s().p("AjTDVQhYhZAAh8QAAh7BYhYQBYhZB7ABQB8gBBYBZQBYBYAAB7QAAB8hYBZQhYBXh8ABQh7gBhYhXg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D2D3D4").s().p("Aj1D3QhnhmAAiRQAAiPBnhmQBmhnCPAAQCQAABmBnQBnBmAACPQAACQhnBnQhmBmiQAAQiPAAhmhmg");

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-34.9,-34.9,69.9,69.9);


(lib.globo = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AANB7IhtAAQgbAAgbgYQgZgYgBgYIAAipQABgXAZgTQAZgTAdAAIDLAAQAcAAAVATQAUASABAYIAACpQAAAWgQARQgPAQgUACIAABIgAhjAcQgFAFgBAIIAAACQABAIAFAGQAGAFAIAAICSAAQAIAAAFgFQAHgGAAgIIAAgCQAAgIgHgFQgFgGgIAAIiSAAQgIAAgGAGgAhjgfQgFAGgBAIIAAABQABAIAFAGQAGAEAIAAIDEAAQAIAAAHgEQAFgGAAgIIAAgBQAAgIgFgGQgGgGgJAAIjEAAQgIAAgGAGgAhjhwQgFAGgBAIIAAABQABAIAFAGQAGAGAIAAIDEAAQAIAAAHgGQAFgGAAgIIAAgBQAAgIgFgGQgGgGgJAAIjEAAQgIAAgGAGg");
	this.shape.setTransform(0.1,1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#789647").s().p("AjTDUQhYhYAAh8QAAh7BYhYQBYhYB7AAQB8AABZBYQBYBYAAB7QAAB8hYBYQhZBYh8AAQh7AAhYhYg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D2D3D4").s().p("Aj2D2QhmhmAAiQQAAiPBmhmQBnhnCPAAQCQAABmBnQBnBmAACPQAACQhnBmQhmBniQAAQiPAAhnhng");

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-34.9,-34.9,69.9,69.9);


(lib.gente = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F8F7F8").s().p("Ag/BZIAAhwQAAgbATgTQATgTAZAAQAaAAATATQATATAAAbIAABwg");
	this.shape.setTransform(0.1,2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F8F7F8").s().p("AgbA+IAAhOQAAgTANgNQANgNARAAQAGAAAGACQgJAQAAASIAABXg");
	this.shape_1.setTransform(-9.3,2.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F8F7F8").s().p("AgTA+IAAhXQAAgSgIgQQAGgCAHAAQAQAAANANQANANAAATIAABOg");
	this.shape_2.setTransform(9.5,2.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F8F7F8").s().p("AgcAcQgLgMAAgQQAAgQALgLQANgMAPAAQARAAALAMQAMALABAQQgBAQgMAMQgLAMgRAAQgPAAgNgMg");
	this.shape_3.setTransform(0.1,-12.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F8F7F8").s().p("AgSArIAAg2QAAgNAJgJQAJgJAKAAIAJABQgIAOAAAOIAAA4g");
	this.shape_4.setTransform(-14.4,1.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F8F7F8").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
	this.shape_5.setTransform(-7.8,-7.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F8F7F8").s().p("AgKArIAAg4QAAgOgIgOIAJgBQAKAAAJAJQAJAJAAANIAAA2g");
	this.shape_6.setTransform(14.6,1.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F8F7F8").s().p("AgSATQgIgIAAgLQAAgKAIgIQAIgIAKAAQALAAAIAIQAIAIAAAKQAAALgIAIQgIAIgLAAQgKAAgIgIg");
	this.shape_7.setTransform(7.9,-7.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F8F7F8").s().p("AgLANQgGgGAAgHQAAgGAGgGQAFgFAGAAQAHAAAGAFQAFAGAAAGQAAAHgFAGQgGAFgHAAQgGAAgFgFg");
	this.shape_8.setTransform(-13.3,-5.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F8F7F8").s().p("AgMANQgFgGAAgHQAAgGAFgGQAGgFAGAAQAHAAAGAFQAFAGAAAGQAAAHgFAGQgGAFgHAAQgGAAgGgFg");
	this.shape_9.setTransform(13.5,-5.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#30AADF").s().p("AiVCWQg+g/AAhXQAAhWA+g/QA/g+BWAAQBYAAA+A+QA+A/AABWQAABXg+A/Qg+A+hYAAQhWAAg/g+g");

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D2D3D4").s().p("AisCuQhJhIAAhmQAAhlBJhIQBIhIBkAAQBmAABIBIQBIBIAABlQAABmhIBIQhIBIhmAAQhkAAhIhIg");

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,49.3,49.3);


(lib.fondo = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#E6E5E6","#FFFFFF"],[0,1],0,-168.8,0,168.9).s().p("AqQYUQkviBjqjpQjpjriBkvQiEk5AAlXQAAlWCEk6QCBkuDpjrQDqjpEviBQE6iEFWAAQFXAAE6CEQEvCBDqDpQDpDrCBEuQCEE6AAFWQAAFXiEE5QiBEwjpDqQjqDpkvCBQk6CElXAAQlWAAk6iEg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-168.8,-168.8,337.8,337.7);


(lib.Group_1_0 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D3D3D3").ss(4,1,1).p("Ag+hiIAABDIBIAAQAUAAAMgHQAMgIAAgTQAAgVgQgHQgOgFgWAAgABlh2QAOATAAAbQAAAcgOARQgJALgOAGQAXAJAMAQQAMASAAAbQAAAbgOAWQgIANgNALQgOALgVAEQgTAEgYAAIiHAAIAAkvICRAAQA4ABAXAggAg+ASIAABRIBIAAQATAAAMgFQAUgKAAgcQAAgYgUgJQgMgFgSAAg");
	this.shape.setTransform(261.7,18.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#020303").s().p("Ah8CYIAAkvICSAAQA3ABAYAfQAOAUAAAbQAAAcgOARQgKAKgNAHQAXAJAMAQQALASAAAbQAAAbgNAWQgIANgOAKQgOAMgUADQgUAFgXAAgAg/BjIBIAAQAUAAAMgGQATgKABgbQAAgZgUgIQgMgGgTABIhJAAgAg/gfIBIAAQAVAAAMgIQAMgHAAgTQAAgVgQgIQgOgEgWAAIhBAAg");
	this.shape_1.setTransform(261.7,18.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#D3D3D3").ss(4,1,1).p("AhAA6QADAVAJALQAQATAkAAQAVAAAQgFQAcgKAAgaQAAgPgOgIQgNgIgdgGIgfgIQgwgKgTgNQgfgTAAgtQAAgpAfgbQAegbA6AAQAvAAAiAZQAjAaABAwIg9AAQgBgagXgNQgQgHgUAAQgZAAgQAKQgPAKAAARQAAARAPAIQAMAGAdAGIAzANQAkAJARALQAbAWAAAoQAAAqggAcQggAbg7AAQg7AAgigbQgjgbAAgwg");
	this.shape_2.setTransform(232,18.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#020303").s().p("AhaCFQgjgbAAgwIA9AAQADAVAIALQAQATAlAAQAVAAAQgFQAbgKAAgaQAAgPgNgIQgNgIgdgGIgfgIQgwgKgTgNQgfgTAAgtQAAgpAegbQAegbA7AAQAuAAAjAZQAjAaABAwIg9AAQgCgagWgNQgQgHgVAAQgYAAgQAKQgPAKAAARQAAARAPAIQALAGAdAGIA0ANQAkAJAQALQAcAWAAAoQAAAqggAcQghAbg6AAQg7AAgigbg");
	this.shape_3.setTransform(232,18.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#D3D3D3").ss(4,1,1).p("AB7iXIAAA2IhcAAIAAD5Ig9AAIAAj5IhcAAIAAg2g");
	this.shape_4.setTransform(204.8,18.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#020303").s().p("AgfCYIAAj5IhbAAIAAg2ID1AAIAAA2IhbAAIAAD5g");
	this.shape_5.setTransform(204.8,18.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#D3D3D3").ss(4,1,1).p("AAnA0QAOgSAAgiQAAgggOgSQgOgTgZAAQgZAAgNATQgOARAAAhQAAAiAOASQANASAZAAQAZAAAOgSgABWBVQgcAjg6AAQg5AAgcgjQgdgjAAgyQAAgvAdgkQAcgkA5AAQA6AAAcAkQAdAkAAAvQAAAygdAjg");
	this.shape_6.setTransform(166.9,22.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#020303").s().p("AhVBVQgdgjAAgyQAAgvAdgkQAcgkA5AAQA6AAAcAkQAdAkAAAvQAAAygdAjQgcAjg6AAQg5AAgcgjgAgmgyQgOARAAAhQAAAiAOASQANASAZAAQAZAAAOgSQAOgSAAgiQAAgggOgSQgOgTgZAAQgZAAgNATg");
	this.shape_7.setTransform(166.9,22.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#D3D3D3").ss(4,1,1).p("Ag9gmIAAgqIAfAAIAAg/IA4AAIAAA/IAlAAIAAAqIglAAIAAB1QAAAOAEADQAEAEARAAIAMgBIAAAsIgcABQgnACgQgQQgKgLAAgVIAAiIg");
	this.shape_8.setTransform(146.7,19.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#020303").s().p("AgUCCQgLgLAAgVIAAiIIgfAAIAAgqIAfAAIAAg/IA5AAIAAA/IAlAAIAAAqIglAAIAAB1QAAAOADADQAFAEARAAIAMgBIAAAsIgcABIgHAAQgiAAgOgOg");
	this.shape_9.setTransform(146.7,19.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#D3D3D3").ss(4,1,1).p("AArggQgCgNgGgJQgKgOgVAAQgcAAgLAeQgGAQAAAXQAAAZAGAPQALAcAbAAQAVAAAIgLQAJgLACgSIA8AAQgCAbgRAYQgdAng2AAQg0AAgaghQgaggAAgzQAAg4AdggQAcghAwAAQAqAAAbATQAcATAFAwg");
	this.shape_10.setTransform(128.3,22.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#020303").s().p("AhNBWQgaggAAgzQAAg4AdggQAcghAwAAQAqAAAbATQAcATAFAwIg9AAQgCgNgGgJQgKgOgVAAQgcAAgLAeQgGAQAAAXQAAAZAGAPQALAcAbAAQAVAAAIgLQAJgLACgSIA8AAQgCAbgSAYQgcAng2AAQgzAAgbghg");
	this.shape_11.setTransform(128.3,22.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#D3D3D3").ss(4,1,1).p("AA0hrQAYAKAPAXQAPAUADAbQADASgBAYIiiAAQABAiAXAOQANAIARAAQAVAAAMgKQAHgGAFgJIA8AAQgCAUgUAUQgeAhg3AAQgrAAgigbQgigcAAg/QAAg4AfggQAfgfAwAAQAdAAAXALgAgig5QgMANgEAWIBkAAQgCgYgOgLQgOgNgUAAQgVAAgNANg");
	this.shape_12.setTransform(105,22.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#020303").s().p("AhMBcQgigcAAg/QAAg4AfggQAfgfAwAAQAdAAAXALQAYAKAPAXQAPATADAbQADATgBAYIiiAAQABAiAXAOQANAIARAAQAVAAAMgKQAHgGAFgJIA8AAQgCATgUAVQgeAhg3AAQgrAAgigbgAgig5QgMAMgEAXIBkAAQgCgYgOgMQgOgMgUABQgVgBgNANg");
	this.shape_13.setTransform(105,22.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#D3D3D3").ss(4,1,1).p("AhPBuIAIAAQAFABALgBQAJgCAEgDQAGgDAEgMQAFgLgBgCIhTjqIBCAAIAvCkIAuikIA/AAIhNDcQgXBBgLAOQgMAQgoAAIgbgBg");
	this.shape_14.setTransform(80.9,26.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#020303").s().p("AhOCdIAAgvIAHAAIARAAQAIgCAEgDQAGgDAEgMQAFgLgBgCIhTjqIBCAAIAvCkIAuikIA/AAIhNDcQgXBBgLAOQgMAQgoAAg");
	this.shape_15.setTransform(80.9,26.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#D3D3D3").ss(4,1,1).p("ABWBVQgcAjg6AAQg5AAgcgjQgdgjAAgyQAAgvAdgkQAcgkA5AAQA6AAAcAkQAdAkAAAvQAAAygdAjgAAnA0QAOgSAAgiQAAghgOgRQgOgTgZAAQgZAAgOATQgOASAAAgQAAAhAOATQAOASAZAAQAZAAAOgSg");
	this.shape_16.setTransform(56.3,22.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#020303").s().p("AhVBVQgdgjAAgyQAAgvAdgkQAcgkA5AAQA6AAAcAkQAdAkAAAvQAAAygdAjQgcAjg6AAQg5AAgcgjgAgngyQgOASAAAgQAAAhAOATQAOASAZAAQAZAAAOgSQAOgSAAgiQAAghgOgRQgOgTgZAAQgZAAgOATg");
	this.shape_17.setTransform(56.3,22.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#D3D3D3").ss(4,1,1).p("AA7hyIAGAAIAAA9IgRgBQgkAAgMAXQgFANAAAZIAABsIg7AAIAAjgIA4AAIAAAnQALgWALgIQAQgOAbAAg");
	this.shape_18.setTransform(36.2,21.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#020303").s().p("AhABzIAAjfIA4AAIAAAnQALgWALgJQAQgOAbAAIACAAIAGAAIAAA9IgRgBQgkAAgMAXQgFANAAAZIAABsg");
	this.shape_19.setTransform(36.2,21.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#D3D3D3").ss(4,1,1).p("ABYAUQgbAWgyAAIg9AAIAABuIhAAAIAAkvICBAAQAtAAAcAXQAbAYAAAyQAAA2gbAUgAAnhYQgNgKgVAAIg3AAIAABZIA3AAQAXAAALgKQAMgLAAgYQAAgXgMgLg");
	this.shape_20.setTransform(13.5,18.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#020303").s().p("AhyCYIAAkvICCAAQAsAAAcAXQAbAYAAAxQgBA3gaAUQgbAWgyAAIg9AAIAABugAgygIIA4AAQAVAAAMgMQANgKAAgYQAAgYgNgKQgNgKgUAAIg4AAg");
	this.shape_21.setTransform(13.5,18.2);

	this.addChild(this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,276.2,44.5);


(lib.Group_1 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhmH3QFGn3jcn5QASAdAUAnQCmHLkcHMIgXAYg");
	this.shape.setTransform(131.8,85.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgirfIAMAFQCiLai1LcIgMAFQC5rhimrfg");
	this.shape_1.setTransform(101.5,79.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEMTIAA4kIAEgBIAFABIAAYkg");
	this.shape_2.setTransform(71.1,78.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAqLcQi1rcCirbIAMgFQilLgC4Lhg");
	this.shape_3.setTransform(40.7,79.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("ABNHiQkcnMCmnMQARgiAVghQjcH5FGH3IgDADIgXgYg");
	this.shape_4.setTransform(10.3,85.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,142.2,157.5);


(lib.Group = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AnDhRIgXgYIADgEQHXFcHbjqQgkAZgcAQQieBAifAAQkQAAkRi/g");
	this.shape.setTransform(67.7,140.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AqugsIgEgMQKzDEKyiwIgFAMQlFBRlFAAQloAAlqhlg");
	this.shape_1.setTransform(73.5,108.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ArgAFIgBgFIABgEIXCAAIAAAJg");
	this.shape_2.setTransform(73.9,75.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AquAtQKujCKuCuIAFANQqzixqyDFg");
	this.shape_3.setTransform(73.5,43.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AnaBqIAXgYQGvkvGvCxQAgASAgAWQjth0juAcQjsAdjrCtg");
	this.shape_4.setTransform(67.7,11);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,147.7,151.6);


(lib.CompoundPath_4 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5C5C60").s().p("AgLRnIAAgJIATAAIAAAJgAAcReIAVAAIAAAIIgUAAgAg0RmIAAgIIAVAAIgBAIIgUAAgABFRdIAVgCIABAJIgVABgAheRjIABgIIAVACIgBAIgABuRaIAVgDIABAIIgVACgAiHRfIABgJIAVADIgBAIgACXRUIAVgCIABAIIgVADgAiwRZIACgIIAUADIgBAIgADAROIAUgDIACAIIgVADgAjZRSIACgIIAUADIgBAIgADoRGIAUgEIACAIIgUAEgAkBRJIACgIIAUAEIgCAJgAEQQ9IAUgFIACAIIgUAFgAkpQ/IACgIIAUAFIgCAIgAE4QyIAUgFIACAHIgUAGgAlRQzIADgIIATAGIgCAIgAFfQnIAUgIIADAIIgUAHgAl4QmIADgHIATAGIgDAIgAGGQYIAUgHIADAIIgUAHgAmfQXIADgHIATAHIgDAIgAGtQKIATgIIADAHIgTAIgAnFQHIADgHIATAIIgDAIgAHSP5IATgJIAEAIIgTAJgAnrP3IAEgIIATAJIgEAHgAH4PnIASgJIAEAHIgTAKgAoPPjIAEgGIASAJIgEAHgAIcPUIASgKIAEAHIgSAKgAozPPIAEgHIASALIgEAGgAJAO/IARgLIAFAHIgSALgApXO6IAFgHIARALIgEAHgAJjOqIARgMIAEAGIgRAMgAp5OjIAEgGIARALIgEAHgAKFOSIAQgMIAFAHIgRAMgAqbOMIAFgHIAQAMIgEAHgAKmN6IAQgNIAFAGIgQANgAq8NzIAFgHIAQANIgFAGgALGNhIAQgOIAFAGIgQAOgArcNYIAGgGIAPANIgFAGgALlNGIAQgOIAFAGIgPAOgAr7M9IAGgHIAPAPIgFAFgAMEMqIAOgPIAGAHIgPANgAsZMgIAGgGIAPAPIgGAFgAMhMNIAOgPIAGAGIgOAOgAs2MCIAGgFIAPAOIgGAGIgPgPgAM9LvIAOgPIAGAFIgOAQgAtRLkIAGgGIAOAPIgHAGgANYLQIANgQIAHAGIgOAPgAtsLDIAGgEIAOAQIgHAEgANyKwIAMgRIAHAFIgNARgAuFKjIAGgFIANAQIgHAGgAOLKPIALgQIAHAEIgMARgAueKBIAHgEIAMAQIgHAFgAOiJuIALgRIAHAEIgLARgAu1JfIAHgEIAMARIgHAEgAO4JLIALgSIAHAFIgLASgAvKI7IAHgDIAKARIgHAEgAPNIoIAKgSIAHAEIgKASgAvfIXIAHgDIAKASIgHADgAPhIDIAJgSIAHAEIgJATgAvyHyIAHgDIAKATIgIADgAPzHfIAJgTIAHAEIgIASgAwEHNIAIgDIAIATIgHADgAQEG6IAIgTIAIADIgIATgAwUGnIAIgEIAIAUIgIADgAQTGUIAIgTIAHADIgHASgAwjGAIAIgDIAHATIgIAEgAQiFtIAGgUIAIADIgHAUgAwwFYIAIgCIAGAUIgIACgAQuFGIAGgTIAIABIgGAUgAw8ExIAIgDIAFAVIgIACgAQ6EfIAFgVIAIADIgFAUgAxHEJIAIgCIAFAUIgIACgARDD2IAFgUIAIACIgFAUgAxQDhIAIgCIAFAUIgIACgARMDPIADgVIAIACIgDAUgAxXC3IAIgBIADAUIgIACgARSClIADgUIAJABIgDAVgAxdCPIAIgBIADAUIgIABgARYB9IACgVIAIACIgCAUgAxiBlIAJAAIACAUIgJABgARcBUIABgUIAIAAIgBAUgAxlA8IAJAAIABAVIgIAAgAReArIABgVIAIAAIgBAWgAxmATIAIAAIABAUIgIAAgARfACIAAgVIAIgBIAAAWgAxmAAIAAgVIAIAAIAAAVgARdg8IAIAAIABAUIgIAAgAxlgqIABgUIAIABIgBAUgARahmIAIAAIACAVIgIAAgAxjhTIACgVIAIABIgCAVgARWiOIAIgBIACAUIgIABgAxfh8IACgUIAIABIgCAUgARQi3IAIgBIADAUIgIACgAxailIADgVIAIACIgDAUgARIjfIAIgCIAEAVIgIABgAxTjOIAEgUIAIACIgEAUgAQ/kIIAIgCIAFAVIgIACgAxLj3IAFgUIAIACIgFAUgAQ1kvIAIgCIAFAUIgIABgAxBkeIAFgUIAIACIgFAUgAQplWIAIgDIAGATIgIADgAw2lHIAGgTIAIACIgGAUgAQcl+IAHgCIAHATIgIADgAwpluIAHgTIAIACIgHAUgAQNmkIAHgDIAIATIgIADgAwbmVIAIgTIAHADIgHATgAP8nKIAIgEIAIATIgHAEgAwLm7IAIgTIAIAEIgJASgAPrnvIAHgEIAJASIgHAFgAv6nhIAJgTIAHAFIgJASgAPYoUIAHgEIAKASIgIAFgAvooGIAKgSIAHAEIgKASgAPEo3IAHgFIAKASIgHAEgAvUoqIAKgSIAHAFIgKARgAu/pOIALgRIAHAFIgLARgAOupaIAHgFIALARIgHAFgAuppxIAMgRIAHAGIgMARgAOXp9IAHgFIALARIgGAFgAuRqTIAMgQIAHAFIgNAQgAN/qfIAGgEIANAQIgHAFgANmq/IAGgFIANARIgHAEgAt4qzIANgRIAGAGIgNAPgAterUIANgPIAGAFIgNAQgANLreIAGgGIAOAQIgHAGIgNgQgAtDryIAOgQIAGAGIgOAPgAMwr8IAGgHIAOAQIgHAFgAsnsRIAPgOIAGAFIgPAPgAMTsaIAGgGIAOAOIgGAGgAsJsuIAPgOIAFAGIgPAOgAL1s3IAGgFIAPANIgGAHgArrtKIAQgNIAFAFIgPAOgALWtSIAGgGIAPAOIgFAGgArLtlIAQgNIAFAGIgQAOgAK3tsIAFgGIAQAMIgFAHgAKWuFIAFgGIAQAMIgFAHgAqrt/IARgMIAEAHIgQAMgAqKuYIARgMIAFAIIgRALgAJ0ucIAFgIIARAMIgFAHgAJSuzIAFgHIARALIgFAHgApnuvIARgLIAFAHIgSALgAIvvIIAEgIIASALIgEAHgApEvFIASgLIAEAIIgSAKgAILvcIAEgHIASAJIgEAIgAogvaIASgJIAEAHIgSAKgAHnvuIADgIIATAJIgEAIgAn8vtIATgJIAEAHIgTAKgAHBwAIAEgIIATAJIgEAIgAnWv/IATgJIADAIIgTAJgAGcwQIADgIIATAIIgDAIgAmwwQIATgIIADAIIgTAIgAF1weIADgIIATAHIgDAIgAmKwfIAUgHIADAHIgUAHgAFOwrIADgIIATAHIgCAIgAliwtIATgHIADAIIgUAHgAEnw2IACgIIAUAFIgDAIgAk7w6IAUgFIACAIIgTAFgAD/xAIACgIIAUAEIgCAIgAkTxEIAUgFIACAIIgUAFgADXxJIABgIIAUADIgBAJgAjqxOIAUgDIABAIIgUADgACuxQIABgIIAVADIgCAIgAjCxWIAVgDIABAJIgUACgACFxWIABgIIAVADIgBAIgAiZxcIAVgDIABAJIgVADgABcxaIABgIIAVABIgBAJgAhwxhIAVgBIABAIIgVACgAAzxdIABgIIAUABIAAAJgAhGxkIAUgBIABAIIgVACgAAKxeIAAgIIAVAAIAAAIgAgdxmIAVAAIAAAIIgVAAg");
	this.shape.setTransform(112.8,112.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,225.6,225.5);


(lib.CompoundPath_2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#040505").s().p("AARCXIgRidIAAgEQgBAAAAgBQAAAAgBAAQAAgBAAAAQgBAAAAAAIgDAAIg5AhIgPBAIgSAMQAAAAAAAAQAAAAgBAAQAAAAgBAAQAAAAgBAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQAAAAAAgBIgBgEIgCglIgBgDIgCgBQgDgBgDACIgFAEQgGADgJgBQgIgCACgIIACgHIAFgGIAIgFIAAAAIAAgBIACgCIgBgDIgCgCIgjgQQgDgCAAgDIABAAIAAgCIARgLIACgBIACAAIA7AMIACAAIACgBIA2gmIABgCQAAgDgCgCIiNhRQgBgBAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIAAgCIABgCIABgCIAOgJIACgBIACAAIDSAsIBAgsQAHgEAIgCQALgBAGACQAFAAAEAEQAEADAAAHIgBAFQgBAHgEAHQgDAHgGAEIhEAqQAAAAgBAAQAAAAAAAAQAAAAAAABQAAAAAAABIgrDKIgDAIIgBABIgBABIgNAJIgEABQgEAAAAgFg");
	this.shape.setTransform(17,15.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,34.2,31.4);


(lib.barras = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FDFEFF").s().p("AirCIQgDgIADgIQAEgHAIgDQApgPA1ghQBog/BBhYIgiANIgHACQgOAAgFgNQgDgIAEgHQADgIAIgDIBYghQAHgDAIADQAHACAEAHQADAFAAAIIAABdQAAAIgHAGQgHAGgJAAQgJAAgHgGQgHgGAAgIIAAgTQhIBWhnA/Qg1AggsAQQgDACgEAAQgOAAgFgNg");
	this.shape.setTransform(-1.1,-8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FDFEFF").s().p("AgCCLQgLAAgHgIQgHgIAAgKIAAjhQAAgKAHgIQAHgIALAAIADAAQALAAAIAIQAIAIAAAKIAADhQAAAKgIAIQgIAIgLAAg");
	this.shape_1.setTransform(13.4,7.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FDFEFF").s().p("AgBBtQgLAAgHgIQgIgHAAgLIAAimQAAgKAIgHQAHgIALAAIADAAQALAAAHAIQAIAHAAAKIAACmQAAALgIAHQgHAIgLAAg");
	this.shape_2.setTransform(6.2,10.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FDFEFF").s().p("AAABRQgLAAgHgHQgJgHAAgKIAAhtQAAgLAJgIQAHgJALAAIAFAAQAKAAAHAJQAGAIAAALIAABtQAAAKgGAHQgGAHgLAAg");
	this.shape_3.setTransform(-0.5,13.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FDFEFF").s().p("AgFA4QgLgBgHgGQgHgGAAgLIAAhBQAAgWAZAAIAGAAQAMAAAIAGQAKAFAAALIAABBQAAAKgKAHQgIAGgMABg");
	this.shape_4.setTransform(-7.5,15.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FDFEFF").s().p("AgBAZQgMAAgJgGQgKgHAAgKIAAgHQAAgJAKgGQAIgEANAAIAGAAQAcAAAAATIAAAHQAAAKgIAHQgJAGgLAAg");
	this.shape_5.setTransform(-14.5,18.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#1971B6").s().p("Aj8D9QhphpAAiUQAAiTBphoQBphpCTAAQCUAABoBpQBqBogBCTQABCUhqBpQhoBoiUAAQiTAAhphog");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D2D3D4").s().p("AkkElQh6h5AAisQAAiqB6h6QB5h6CrAAQCsAAB6B6QB5B6AACqQAACsh5B5Qh6B6isAAQirAAh5h6g");

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-41.5,-41.5,83.1,83.1);


(lib.punteada = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.CompoundPath_4();
	this.instance.setTransform(0.1,0.1,1,1,0,0,0,112.8,112.8);
	this.instance.alpha = 0.551;

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-112.7,-112.7,225.6,225.5);


(lib.proyecto = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1971B6").s().p("Ah7CYIAAkvICQAAQA4ABAYAgQANASAAAcQAAAcgNARQgJAJgPAIQAXAJAMAQQANATAAAaQAAAbgOAVQgJAPgMAJQgPAMgUAEQgUAEgYAAgAg/BjIBIAAQATAAAMgFQAUgKAAgdQAAgXgUgJQgKgFgUgBIhJAAgAg/gfIBJAAQATAAAMgHQANgIAAgTQAAgVgQgHQgOgFgWAAIhBAAg");
	this.shape.setTransform(123.9,-4.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1971B6").s().p("AhaCFQgjgbAAgwIA9AAQADAVAJALQAQATAkAAQAVAAAQgFQAbgKAAgaQAAgPgNgIQgNgIgdgGIgfgIQgwgKgTgNQgfgTAAgtQAAgpAegbQAegbA7AAQAvAAAiAZQAjAaABAwIg9AAQgCgbgXgMQgPgHgVAAQgYAAgQAKQgPAKAAARQAAARAPAIQALAGAdAGIA0ANQAkAJARALQAbAWAAAoQAAAqggAcQggAbg8AAQg6AAgigbg");
	this.shape_1.setTransform(94.1,-4.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#1971B6").s().p("AgfCYIAAj5IhbAAIAAg2ID1AAIAAA2IhbAAIAAD5g");
	this.shape_2.setTransform(66.9,-4.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1971B6").s().p("AhVBVQgdgjAAgyQAAgvAdgkQAcgkA5AAQA5AAAdAkQAdAkAAAvQAAAygdAjQgdAjg5AAQg5AAgcgjgAgngyQgOARAAAhQAAAiAOASQAOASAZAAQAZAAAOgSQAOgSAAgiQAAghgOgRQgOgTgZAAQgZAAgOATg");
	this.shape_3.setTransform(29.1,-0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1971B6").s().p("AgUCCQgKgKAAgWIAAiIIggAAIAAgqIAgAAIAAg/IA4AAIAAA/IAlAAIAAAqIglAAIAAB1QAAAOAEADQADAEASAAIAMgBIAAAsIgcABIgHAAQgjAAgNgOg");
	this.shape_4.setTransform(8.9,-3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1971B6").s().p("AhNBWQgaghAAgyQAAg4AdggQAcghAwAAQAqAAAcATQAaATAGAwIg9AAQgBgLgHgLQgKgOgVAAQgcAAgLAeQgGAQAAAXQAAAZAGAPQAKAcAcAAQAUAAAJgLQAJgLACgSIA8AAQgCAbgSAYQgcAng2AAQg0AAgaghg");
	this.shape_5.setTransform(-9.6,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#1971B6").s().p("AhMBbQgigbAAg/QAAg4AfggQAggfAvAAQAdAAAYAKQAXALAQAXQANAUAFAaQACAQgBAbIiiAAQACAiAWANQANAJARAAQAVAAAMgKQAHgGAFgKIA8AAQgCAVgTAUQgfAhg2AAQgsAAgigcgAgig4QgMANgDAWIBjAAQgBgYgOgMQgOgNgVAAQgVAAgNAOg");
	this.shape_6.setTransform(-32.9,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1971B6").s().p("AhOCdIAAgvIAHAAIARAAQAIgCAFgDQAFgDAEgMQAFgLAAgCIhUjqIBCAAIAvCkIAvikIA+AAIhMDcQgXBAgMAPQgMAQgnAAg");
	this.shape_7.setTransform(-56.9,4.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#1971B6").s().p("AhVBVQgdgjAAgyQAAgvAdgkQAcgkA5AAQA6AAAcAkQAdAkAAAvQAAAygdAjQgcAjg6AAQg5AAgcgjgAgmgyQgOARAAAhQAAAiAOASQANASAZAAQAZAAAOgSQAOgTAAghQAAgggOgSQgOgTgZAAQgZAAgNATg");
	this.shape_8.setTransform(-81.6,-0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#1971B6").s().p("AhABzIAAjgIA5AAIAAAnQALgVAKgJQAQgOAbAAIADAAIAFABIAAA8IgRgBQgjAAgNAXQgEANAAAZIAABsg");
	this.shape_9.setTransform(-101.7,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#1971B6").s().p("AhxCYIAAkvICAAAQAtAAAbAYQAbAXAAAxQAAA2gbAVQgaAWgyAAIg9AAIAABugAgygJIA3AAQAWABAMgLQAMgLAAgYQAAgXgMgLQgNgKgVAAIg3AAg");
	this.shape_10.setTransform(-124.3,-4.1);

	this.instance = new lib.Group_1_0();
	this.instance.setTransform(-0.2,0,1,1,0,0,0,138.1,22.2);
	this.instance.alpha = 0.648;

	this.addChild(this.instance,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-138.3,-22.2,276.2,44.5);


(lib.mundo = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#3979BA","#FFFFFF"],[0,1],55.7,104.2,-43.5,-49.5).s().p("AFCK5Qklg4k1klQlQk8hLlAQgiiRAhhtQAihvBfg2IAzAzQhCA6gPBkQgPBjAmB7QBTEREYEGQEADyD4BDQDrA+B0h1IA9A9Qg1BUhlAgQg4AShDAAQg0AAg6gLg");
	this.shape.setTransform(-1.4,-4.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#3979BA","#FFFFFF"],[0,1],61.4,101.8,-36.3,-49.6).s().p("AL/F6QAdhVgjheQgjhdhbhWQjKi7lwhLQlPhEj2BPQjnBJgsCjIhSgSQAFhlBIhTQBHhSB+gyQEXhwGWBTQDcAsCuBYQCjBTBmBuQBiBoAYBwQAZByg4Big");
	this.shape_1.setTransform(0,-8.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#3979BA","#FFFFFF"],[0,1],60.7,83.7,-35,-64.7).s().p("AotG7QiIgkhLhTQhNhTAAhzIBDgYQARBZBMA9QBKA8B4AXQEJAyFdiMQFBiACsjJQCii/grilIBQgdQAtBagVBuQgVBuhTBxQi5D3mECcQkZBxjlAAQhvAAhigbg");
	this.shape_2.setTransform(-0.5,9.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ADNIUQhwgnh0hJQifAsiPAFQiHAFhtgfQhEhUgshkQBTBACGASQCMAUCpggQhHg3hDg/QiNiFhkiNQhdiHgviEQgbAUgWAYQAkiKBQhzIAEAiQCDg1CrgKQC4gKDMAqQDHAoCjBMQCXBHBoBgQAUBXAABeIgBAlQhXBxiLBjQiUBpi/BMIgfAMQB9A+BwAOQBtAPBQggQhABRhRA9QhfgDhsgkgAkmnAQigAJh3A0QAqB7BWB/QBdCICFB7QBsBkBpBGQA+gUBAgZQC5hKCNhoQCEhgBNhvQhehfiRhHQibhMjCgoQiWgeiKAAQglAAgkACg");
	this.shape_3.setTransform(1.2,8.6);

	this.instance = new lib.Group();
	this.instance.setTransform(-0.2,1.2,1,1,0,0,0,73.9,75.7);
	this.instance.alpha = 0.48;

	this.instance_1 = new lib.Group_1();
	this.instance_1.setTransform(-0.3,1.2,1,1,0,0,0,71,78.7);
	this.instance_1.alpha = 0.48;

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#04BCE0","#1971B6"],[0,1],-45.5,-47.7,41.5,43.5).s().p("AkeLVQiEg8hnhtQhmhtg4iNQg6iSAAigQAAieA6iTQA4iNBmhtQBnhtCEg8QCKg+CUAAQCWAACJA+QCEA8BnBtQBmBtA4CNQA6CSAACfQAACgg6CSQg4CNhmBtQhnBtiEA8QiJA+iWAAQiUAAiKg+g");
	this.shape_4.setTransform(-0.2,1.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#3979BA","#FFFFFF"],[0,1],100.2,132.7,-6.1,-32.1).s().p("AjUDxQAYixC8i1QBghfB4hKQhkBChSBQQipCkgQCeQgFApAIAoIhDAYQAAgWADgYg");
	this.shape_5.setTransform(-63.5,-6.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#3979BA","#FFFFFF"],[0,1],56.7,109.4,-39.5,-39.8).s().p("ADRACQhuhqjmgFQhugDhyATQCHgbCDADQD9AGB+BzQAqAnAYAvIhQAcQgRhCgygyg");
	this.shape_6.setTransform(45.4,-46.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#3979BA","#FFFFFF"],[0,1],101.1,151.1,3.8,0.3).s().p("ACGgLQjZhSiKA7QgkAPgeAYIgzgyQATgKAVgJQCeg9DuBcQB8AtB1BNQhlg+hogmg");
	this.shape_7.setTransform(-23.9,-70.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#3979BA","#FFFFFF"],[0,1],33.4,56.8,-66.9,-98.6).s().p("AABE4QAvgxARhGQAmiahnjbQguhjhAhcQBQBsA3B2QBzDxgmCuQgLA4gdAvg");
	this.shape_8.setTransform(63.4,15.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#3979BA","#FFFFFF"],[0,1],17.2,45,-76.6,-100.3).s().p("AlECGQB1gCBugXQDhgvBbh8QAYgiANglIBFAQQgKATgPATQhqCJj5A0Qh0AYh7AAIgeAAg");
	this.shape_9.setTransform(51.1,42.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#3979BA","#FFFFFF"],[0,1],75.9,109,-22.8,-44).s().p("AATCVQjViSg0ipQgQg3ACg2IBSATQgRBCATBHQApCXDBCFQBeBABtAvQiCgzhwhMg");
	this.shape_10.setTransform(-61.3,17.5);

	this.addChild(this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.instance_1,this.instance,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-87.5,-79.9,175.1,159.8);


(lib.avion = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAXCcQgDgBgBgEIgTidIAAgDIgCgCIgBAAIgCAAIg4AiIgPA/IgRAMIgDAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAgBAAAAIgCgEIgCgmQAAAAAAAAQAAgBAAAAQAAAAAAgBQgBAAAAAAIgCgCQgEAAgCACIgEADQgHAEgJgCQgHgBABgIIADgHIAEgGIAHgFIABgBIACgCIgBgDIgCgCIgjgQQgEgDACgDIAAgBIARgLIADgBIA8AMIACAAQAAAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAAAIA1gmIABgDQACgDgDgCIiOhPQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAgBgBIABgEIABgCIAPgJIACgBIABAAIDSAqIA/gsQAHgFAIgCQAKgBAHACQAFAAAEAEQAEADABAGIgBAGQgCAIgEAGQgDAHgGADIhCArIgCACIgBACIgoDIIgBAEIgBAEIgBABIgOAKIgEABIgBAAg");
	this.shape.setTransform(0.2,-0.6);

	this.instance = new lib.CompoundPath_2();
	this.instance.setTransform(-0.1,0.6,1,1,0,0,0,17,15.7);
	this.instance.alpha = 0.602;

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-17.1,-16.2,34.3,32.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;