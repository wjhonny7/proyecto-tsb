/**
* Anuncio View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var AnuncioView = {};

	// Extends my object from Backbone events
	AnuncioView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'change #categorias': 'selectSubcategorias',
				'click #all_planes': 'showPopupModal',
				'submit #anuncio-form': 'saveAnuncio',
				'click .desactivar-button': 'desactivarAnuncio',
				'click .activar-button': 'publicarAnuncio',
				'click #desactivar-anuncios-button': 'unpublishAnuncios',
				'click #activar-anuncios-button': 'publishAnuncios',
				'click .eliminar': 'deleteAnuncio',
				'click .reactivar': 'reactivarAnuncio'
			}

		,	view: this

		,	subviews: {}

		,	initialize: function(){

				_.bindAll(
					this,
					'selectSubcategorias',
					'onCompleteSelectSubcategorias',
					'showPopupModal',
					'desactivarAnuncio',
					'publicarAnuncio',
					'unpublishAnuncios',
					'publishAnuncios',
					'deleteAnuncio',
					'reactivarAnuncio'
				);

				this.anuncio = new AnuncioModel();
				this.subcategorias = [];

			}

			/**
			* Validate form anuncio and ajax save
			*
			*/
		,	saveAnuncio: function( e ){

				e.preventDefault();

				
				this.subviews.imageUploader = window.imageUploadView;
			

				var data = utilities.formToJson( '#anuncio-form' )
				,	required = [ 
							'titulo'
		        		,	'descripcion_corta'
		        		,	'descripcion_detallada'
		        		,	'razon_social'
		        		,	'precio'
		        		,	'direccion'
					]
				,	errors = [];

				console.log( data );
				
				utilities.validateEmptyFields( required, data, errors );


				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if( ! utilities.justNumbers( data.precio ) )
					return utilities.showNotification( 'error', 'El campo precio solo debe contener números', 0 );

				if( this.subviews.imageUploader.imageQue.length <= 0  )
					return utilities.showNotification( 'error', 'Debe al menos seleccionar una imagen para su anuncio.', 0 );

				if( data.categorias == 'Seleccione')
					return utilities.showNotification( 'error', 'Debe al menos seleccionar una categoria para su anuncio.', 0 );

				if( data.subcategoria == '')
					return utilities.showNotification( 'error', 'Debe al menos seleccionar una Subcategoria para su anuncio.', 0 );

				if( data.plan == 'Seleccione')
					return utilities.showNotification( 'error', 'Debe seleccionar un plan.', 0 );

				if ( data.enlace == 'Seleccione' )
					return utilities.showNotification( 'error', 'Debe seleccionar el enlace o gestor.', 0 );


				data.subcategorias = data.subcategoria.split(',');

				data.imagenes = this.subviews.imageUploader.imageQue;

				this.anuncio.set( data );

				this.anuncio.save( this.onCompleteSaveAnuncio, this.onError );
			}

			/**
			* Function when complete the process save anuncio
			*
			*/
		,	onCompleteSaveAnuncio: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );
				

				$('#anuncio-form')[0].reset();

				$('#form-payu').html(data.form);

				$( '#form-pagos' ).submit();

				return utilities.showNotification( 'success', data.message, 0 );
				
			}

		,	desactivarAnuncio: function( e ){

				e.preventDefault();

				var target = e.currentTarget,
					id = $( target ).data('id');

				this.anuncio.unpublish( id, this.onCompleteDesactivarAnuncio, this.onError );
			}

		,	publicarAnuncio: function( e ){

				e.preventDefault();

				var target = e.currentTarget,
					id = $( target ).data('id');

				this.anuncio.publish( id, this.onCompleteDesactivarAnuncio, this.onError );
			}

		,	unpublishAnuncios: function( e ){

				e.preventDefault();

				var ids = [];


				$.each($('input[name="cid[]"]'), function(index, val) {

					if ( this.checked ) {

						var id = $(this).val();
						ids.push( id );
					};		

				});

				if ( ids.length <= 0 )
					return utilities.showNotification( 'error', 'Seleccione mínimo un anuncio', 0 );

				this.anuncio.unpublishAnuncios( ids, this.onCompleteDesactivarAnuncio, this.onError );

			}

		,	publishAnuncios: function( e ){

				e.preventDefault();

				var ids = [];

				$.each($('input[name="cid[]"]'), function(index, val) {
					
					if ( this.checked ) {

						var id = $(this).val();
						ids.push( id );
					};	
				});



				if ( ids.length <= 0 )
					return utilities.showNotification( 'error', 'Seleccione mínimo un anuncio', 0 );

				this.anuncio.publishAnuncios( ids, this.onCompleteDesactivarAnuncio, this.onError );

			}

		,	deleteAnuncio: function( e ){

				e.preventDefault();

				var target = e.currentTarget,
					id = $( target ).data('id');

				this.anuncio.deleteAnuncio( id, this.onCompleteDesactivarAnuncio, this.onError  );
			}

		,	onCompleteDesactivarAnuncio: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification( 'success', data.message, 1500, function(){
					window.location.reload();
				} );

			}

		,	reactivarAnuncio: function( e ){

				e.preventDefault();

				var current = e.currentTarget,
					id = $(current).data('id'),
					plan = $('#list_planes'+id).val();

				if ( plan == '0' )
					return utilities.showNotification( 'error', 'Debe seleccionar un plan', 0 );

    			var data = { id : id, plan : plan };

    			this.anuncio.reactivarPlan( data, this.onCompleteReactivar, this.onError  );

			}

		,	onCompleteReactivar: function( data ){

				console.log( data );

				$('#form-payu').html(data.form);

				utilities.showNotification( 'success', data.message, 0 );

				$( '#form-pagos' ).submit();


			}





			/**
			* Validate form and redirect the request to the different model
			*
			*/
			
		,	selectSubcategorias: function(e) {

				var id = $('#categorias').val();
				
				this.anuncio.selectSubcategorias( id, this.onCompleteSelectSubcategorias, this.onError );
				

			}

		,	onCompleteSelectSubcategorias: function( data ){

				var _this = this;

				if( data.status == 500 ){

					$( '#subcategorias' ).find('option').remove();
					$( "#subcategorias" ).trigger("change");
					return utilities.showNotification( 'error', data.message, 0 );
					
				}

				_this.subcategorias = [];

				$.each(data.categorias, function(index, item) {

					_this.subcategorias.push( item.category_name );

				});


				$('#subcategorias').select2({ tags: this.subcategorias });

			}

		,	onLoadPlanesModal: function(){

				var planes = [];

				this.anuncio.getPlanes(function( data ){ 

					Planess = data.planes;

					return data.planes;

				}, this.onError );

				

			}

		,	showPopupModal:function( e ){

				e.preventDefault();

				this.anuncio.getPlanes(function( data ){ 

					
					var modal = {
						title: 'Planes',
						content: new EJS( { url: url + 'js/templates/planes.ejs'}).render( data ),
						height: 450

					};

					utilities.showModalWindow( modal );

					$('.bxslider').bxSlider({
						slideWidth: 570,
					    minSlides: 2,
					    maxSlides: 2,
					    slideMargin: 10,
					    pager: false
					});
					

				}, this.onError );

				
			}
				

		,	onSendEmailForm: function( data ){

				console.log( data );

				if(data.status == 500)
					utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification( 'success', data.message, 0 );

			}

			/**
			* Initializes the datepicker
			*
			*/
		,	initDatePicker: function(){

				var fechas = JSON.parse( $( "#fechas" ).val() );

				utilities.initDatePicker( fechas );

			}				

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( errorThrown );

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.AnuncioView = new AnuncioView();

})( jQuery, this, this.document, this.Misc, undefined );