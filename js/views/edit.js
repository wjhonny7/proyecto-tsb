/**
* Anuncio View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var EditView = {};

	// Extends my object from Backbone events
	EditView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #edit-anuncio-button': 'editAnuncio'
			}

		,	view: this

		,	subviews: {}

		,	initialize: function(){

				_.bindAll(
					this,
					'editAnuncio'
				);

				this.anuncio = new AnuncioModel();
				this.subcategorias = [];

			}

			/**
			* Validate form anuncio and ajax save
			*
			*/
		,	editAnuncio: function( e ){

				e.preventDefault();

				
				this.subviews.imageUploader = window.imageUploadView;
			

				var data = utilities.formToJson( '.edit-anuncio-form' )
				,	required = [ 
					]
				,	errors = [];

				console.log( data );
				
				utilities.validateEmptyFields( required, data, errors );

				if( ! utilities.justNumbers( data.precio ) )
					return utilities.showNotification( 'error', 'El campo precio solo debe contener números', 0 );

				data.subcategorias = data.subcategoria.split(',');

				data.imagenes = this.subviews.imageUploader.imageQue;

				this.anuncio.set( data );

				this.anuncio.save( this.onCompleteSaveAnuncio, this.onError );
			}

			/**
			* Function when complete the process save anuncio
			*
			*/
		,	onCompleteSaveAnuncio: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );
				

				utilities.showNotification( 'success', data.message, 1000, function(){

					window.location.reload();
				} );
				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( errorThrown );

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.EditView = new EditView();

})( jQuery, this, this.document, this.Misc, undefined );