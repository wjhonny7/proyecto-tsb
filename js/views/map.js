/**
* Google Maps View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var MapView = {};

	// Extends my object from Backbone events
	MapView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup #direccion': 'renderAddressOnMap'

			}

		,	view: this

		,	mapOptions: {
				center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

		,	map: {}

		,	geocoder: {}

		,	initialize: function(){

				_.bindAll(
					this, 
					'initMap',
					'initProductMap',
					'addListeners',
					'onClickMap',
					'renderAddressOnMap',
					'placeMarker'
				);

				if( $('#map_canvas').length ){
					this.initMap();
					this.addListeners();
				}

				if( $('#product-map-canvas').length ){

					this.initProductMap();
					
				}			
			}

			/**
			* Initializes the map on the canvas
			*
			*/
		,	initMap: function(){

				var _self = this;



				if( $( '#latlang' ).length ){

					var latlng = $( '#latlang' ).val();

					if( latlng != '' ){

						latlng = latlng.split(',', 2 );

						var lat = parseFloat(latlng[0]);
						var lng = parseFloat(latlng[1]);

						var latlng = new google.maps.LatLng(lat, lng);

						this.mapOptions.center = latlng;


					}
				}

				this.map = new google.maps.Map(
					document.getElementById("map_canvas"),
					this.mapOptions
				);

				this.marker = new google.maps.Marker({
					map: this.map,
					draggable: false
				});

				this.geocoder = new google.maps.Geocoder();

				if( typeof latlng == 'object' ){

					_self.geocoder.geocode( { 'latLng': latlng }, function( results, status ) {

						if ( status == google.maps.GeocoderStatus.OK ) {
							if ( results[1] ) {
								_self.marker = new google.maps.Marker({
									position: latlng,
									map: _self.map
								});

								// infowindow.setContent(results[1].formatted_address);
								// infowindow.open(map, marker);
							} else {
								utilities.showNotification( 'warning', 'No se pudo recuperar la localización del anuncio en el mapa.', 3000);
							}
						} else {
							utilities.showNotification( 'error', 'La geocodificación de google maps debido a: ' + status, 3000 );
						}
					});
				}


			}

			/**
			* Initializes the product map
			*
			*/
		,	initProductMap: function(){


				var latlng = $( '#latlang' ).val();


				latlng = latlng.split(',', 2 );

				var lat = parseFloat(latlng[0]);
				var lng = parseFloat(latlng[1]);

				var latlng = new google.maps.LatLng(lat, lng);

			    var productMapOptions = {
			        zoom: 14,
			        center: latlng,
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    }

				var productMap = new google.maps.Map(
					document.getElementById("product-map-canvas"),
					productMapOptions
				);

				var geocoder = new google.maps.Geocoder();

				geocoder.geocode( { 'latLng': latlng }, function( results, status ) {
					if ( status == google.maps.GeocoderStatus.OK ) {
						if ( results[1] ) {
							marker = new google.maps.Marker({
								position: latlng,
								map: productMap
							});

							// infowindow.setContent(results[1].formatted_address);
							// infowindow.open(map, marker);
						} else {
							utilities.showNotification( 'warning', 'No se pudo recuperar la localización del anuncio en el mapa.', 3000);
						}
					} else {
						utilities.showNotification( 'error', 'La geocodificación de google maps debido a: ' + status, 3000 );
					}
				});

				
			}

			/**
			* Add the map listeners to the event
			*
			*/
		,	addListeners: function(){

				google.maps.event.addListener( this.map, 'click', this.onClickMap );

				return;
			}

			/**
			* Catch the map address when user clicks on the map
			*
			*/
		,	onClickMap: function( e ){

				this.placeMarker( e.latLng );
				this.getAddress( e.latLng.k + ',' + e.latLng.B );
			}

			/**
			* Render the address inserted by user on the map
			*
			*/
		,	renderAddressOnMap: function( e ){

				var target = e.currentTarget;

				var address = $( target ).val();

				var _self = this;

				this.geocoder.geocode( { 'address': address}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						_self.map.setCenter(results[0].geometry.location);
						_self.marker.setPosition( results[0].geometry.location );

					} else {

						return;
					}
				});
			}

			/**
			* Get the read human address from latitud and longitude
			*
			*/
		,	getAddress: function( latLng ){

				var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng + "&sensor=false";

				$( '#latlang' ).val( latLng );

	            jQuery.getJSON(url, function (json) {

	            	console.log( json );
	                
	                var formattedAddress = json.results[0].formatted_address;
	                var departamento = json.results[0].address_components[4].long_name;
	                var city = json.results[0].address_components[3].long_name;
	                var neighborhood = json.results[1].address_components[0].long_name;
	                
	                formattedAddress = formattedAddress.replace( new RegExp("[,\s]+[ña-zA-Z _]+[,\s]+[ña-zA-Z _]+[ña-zA-Z _]+"), '' );

	                $( '#direccion' ).val( formattedAddress );
	                $( '#ciudad' ).val( city );
	                $( '#barrio' ).val( neighborhood );
	                $( '#departamento' ).val( departamento );
	            }); 

			}

			/**
			* Place the marker into the map when user clicks on
			*
			*/
		,	placeMarker: function ( location ){

				this.marker.setPosition( location );
				return;
			}			

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});
	
	$(document).ready(function($) {
		window.MapView = new MapView();	
	});	

})( jQuery, this, this.document, this.Misc, undefined );