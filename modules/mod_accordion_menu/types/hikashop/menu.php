<?php 
/*------------------------------------------------------------------------
# mod_jo_accordion - Vertical Accordion Menu for Joomla 1.5 
# ------------------------------------------------------------------------
# author    Roland Soos 
# copyright Copyright (C) 2011 Offlajn.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.offlajn.com
-------------------------------------------------------------------------*/
?>
<?php
// no direct access
defined('_JEXEC') or die('Restricted access'); 
if(!defined('OfflajnHikashopMenu')) {
  define("OfflajnHikashopMenu", null);
  
  if(!is_dir(JPATH_ROOT.DS.'components'.DS.'com_hikashop'.DS.'controllers')){
    echo JText::_("This component is not installed!");
    return;
  }
  
  require_once(dirname(__FILE__) . DS .'..'.DS.'..'.DS.'core'.DS.'MenuBase.php');

  class OfflajnHikashopMenu extends OfflajnMenuBase{
    
    function OfflajnHikashopMenu($module, $params){
      parent::OfflajnMenuBase($module, $params);
    }
    
    function getAllItems(){
      $db = & JFactory::getDBO();
      $categoryid = $this->_params->get('categoryid');
      $query = "SELECT DISTINCT 
        category_id AS id, 
        category_name AS name, "; 
         if($this->_params->get('displaynumprod', 0) != 0){
          $query.= "(SELECT COUNT(*) FROM #__hikashop_product_category AS ax LEFT JOIN #__hikashop_product AS bp ON ax.product_id = bp.product_id WHERE ax.category_id = id AND bp.product_published=1";
          $query.= ") AS productnum, ";
        }else{
          $query.= "0 AS productnum, ";
        }     
      if(!is_array($categoryid) && $categoryid != 0){
        $query.="IF(category_parent_id = ".$categoryid.", 0 , IF(category_parent_id = 0, -1, category_parent_id)) AS parent, ";
      }elseif(count($categoryid) && is_array($categoryid) && !in_array('0', $categoryid)){
        $query.="IF(category_id in (".implode(',', $categoryid)."), 0 , IF(category_parent_id = 0, -1, category_parent_id)) AS parent, ";
      }else{
        $query.="category_parent_id AS parent, ";
      }
      $query.="'cat' AS typ ";
      $query.= " FROM #__hikashop_category
                WHERE (category_published =1 AND category_type='product') OR (category_type='root' AND category_published =0) ";
      if ($this->_params->get('elementorder', 0) == 0)
        $query.="ORDER BY category_ordering ASC, category_name DESC";
      else if($this->_params->get('elementorder', 0)==1)
        $query.="ORDER BY category_name ASC";
      else if($this->_params->get('elementorder', 0)==2)
        $query.="ORDER BY category_name DESC";
      
      $db->setQuery($query);

      $allItems = $db->loadObjectList('id');
      
      if($this->_params->get('showcontents') == 1){
        $query = "
          SELECT DISTINCT b.product_id, concat( a.category_id, '-', a.product_id ) AS id, b.product_name AS name, a.category_id AS parent, 'prod' AS typ, 0 AS productnum
          FROM #__hikashop_product_category AS a
          LEFT JOIN #__hikashop_product AS b ON a.product_id = b.product_id
          WHERE product_published = 1 ";
        if($this->_params->get('elementorder', 0)==2)
          $query.="ORDER BY product_name DESC";
        else
          $query.="ORDER BY product_name ASC";
        $db->setQuery($query);

        $allItems += $db->loadObjectList('id');
      }
      return $allItems;
    }
    
    function getActiveItem(){  
      $active = null;
      if(JRequest::getVar('option') == 'com_hikashop'){
        $content_id = 0;
        $category_id = 0; 
        if (JRequest::getString('ctrl') == "category") {
          $category_id = JRequest::getInt('cid');        
        } elseif (JRequest::getString('ctrl')=="product") { 
          $content_id = JRequest::getInt('cid');
          $category_id = JRequest::getInt('categoryp');      
        }        
        if($content_id > 0 && $this->_params->get('showcontents')){ 
          $active = new StdClass();         
          $active->id = $category_id."-".$content_id;          
        } elseif ($category_id>0) {
          $active = new StdClass();
          $active->id = $category_id;
        }
      }      
      return $active;
    }
    
    function getItemsTree(){   
      return $this->getItems();
    }
    
    function filterItem(&$item){
      $item->nname = stripslashes($item->name);
      if($this->_params->get('displaynumprod', 0) == 1 && $item->typ == 'cat' && $item->productnum > 0){
        $item->nname.= " (".$item->productnum.")"; 
      }elseif($this->_params->get('displaynumprod', 0) == 2 && $item->typ == 'cat'){
        $item->nname.= " (".$item->productnum.")"; 
      }
      $item->nname = '<span>'.$item->nname.'</span>';
      if($item->typ == 'cat'){
        if($this->_params->get('parentlink') == 0 && $item->p){
          $item->nname = '<a>'.$item->nname.'</a>';
        }else{
          $item->nname = '<a href="'.JRoute::_('index.php?option=com_hikashop&ctrl=category&task=listing&cid='.$item->id).'">'.$item->nname.'</a>';
        }
      }elseif($item->typ == 'prod') {
        $id = explode("-", $item->id);
        $item->nname = '<a href="'.JRoute::_('index.php?option=com_hikashop&ctrl=product&task=show&cid='.$id[1].'&categoryp='.$id[0]).'">'.$item->nname.'</a>';
      }
    }
    
  }
}
?>