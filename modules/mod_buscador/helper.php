<?php
/**
 * Helper class for Hello World! module
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:modules/
 * @license        GNU/GPL, see LICENSE.php
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modBuscadorHelper {
    /**
     * Retrieves the hello message
     *
     * @param array $params An object containing the module parameters
     * @access public
     */    
    
     public static function getCategoriese()  {

        $db = JFactory::getDbo();

        $query = $db ->getQuery(true);
        $query->select('a.*, b.*');
        $query->from('#__virtuemart_categories_es_es AS a');
        $query->innerJoin( '#__virtuemart_category_categories AS b ON b.category_child_id = a.virtuemart_category_id' );
        $query->where( 'b.category_parent_id = 0' );
        
        $db->setQuery( $query );

        return $db->loadObjectList();
    }

}
?>