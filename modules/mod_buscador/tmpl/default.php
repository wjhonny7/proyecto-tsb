<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$data = unserialize( $app->getUserState( 'search' ) );

?>

<div class="modbuscador">
<h3>Buscador</h3>
    <form action="<?= $url ?>index.php?option=com_busqueda&task=busqueda.search" method="post">
       
        <div class="dropdown">
        	<label>Palabra o clave </label>
        	<input type="text" class="clave" name="clave" value="<?= $data['clave'] ?>"/>
        </div>

        <div class="dropdown">
        	<label>Ciudad</label>
				<input type="text" name="ciudad" value="<?= $data['ciudad'] ?>">	

        </div>
        <div class="dropdown">
        	<label>Marca </label>
        	<input type="text" class="marca" value="<?= $data['marca'] ?>"/>
        </div>

        <div class="dropdown">
			<label> Categoría </label>
            <select name="categorias" id="search_categorias">
                <option value=""></option>
                <?php 
                $categorias = modBuscadorHelper::getCategoriese();

                foreach ($categorias as $key => $categoria) {

                     $selected = '';

                    if($data['categorias'] == $categoria->virtuemart_category_id ){
                        $selected = 'selected';
                    }
                ?>
                    <option <?= $selected ?> value="<?= $categoria->virtuemart_category_id ?>"><?= $categoria->category_name ?></option>
                <?php
                }
                ?>
            </select>

        </div>
        
        <input type="submit" name="search" value="Buscar">

    </form>

</div>
