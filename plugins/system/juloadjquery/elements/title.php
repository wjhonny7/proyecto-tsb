<?php
/**
 * ------------------------------------------------------------------------
 * Title field for Joomla admin form
 * ------------------------------------------------------------------------
 * @copyright	Copyright (C) 2010 - 2012 JoomUltra Co., Ltd
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: JoomUltra Co., Ltd
 * Websites: http://www.joomultra.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die ;

jimport('joomla.form.formfield');

class JFormFieldTitle extends JFormField {
		
	public $type = 'Title';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	protected function getLabel() {
		
		$html = '';
		$value = trim($this->element['title']);

		$html .= '<div class="jutitle-field">';
		if ($value) {
			$html .= JText::_($value);
		}
		$html .= '</div>';

		return $html;
	}

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput() {
		return '';
	}

}
?>