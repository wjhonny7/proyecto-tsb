<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
    	<meta charset="utf-8" />

        <!-- Joomla Head -->
		<jdoc:include type="head" />

        <!-- Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

        <!-- Less -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=contenidos">

        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAcZENY952yG1ZgF9b0wRGRc7jtkmfwN0Y&sensor=true"></script>
        <script src="js/libs/select2.js"></script>
		<script type="text/javascript">
          var x;
          x=jQuery(document);
          x.ready(inicio);
          function inicio(){
            var ancho= window.innerWidth;
            var margen= (1900-ancho)/2;
            var x;
            x=jQuery("#banner");
            x.css("margin-left","-"+margen+"px");
          }
        </script>

        <script type="text/javascript">

            var url = '<?php echo JURI::root() ?>';
        </script>

	</head>

    <body onresize="inicio()">

        <header>
            <div id="banner">
                <jdoc:include type="modules" name="banner"  style="xhtml" />
            </div>
            
            <div id="header-center-top">
                <div id="logo-tsb"><iframe width="395" height="436" frameborder="0" src="html5/logo.html" style="background-color:transparent" scrolling="no"></iframe></div>
                <div id="login"><jdoc:include type="modules" name="login"  style="xhtml" /></div>
            </div>
        </header>

        <div id="warp-cont">
            <main id="content">
                <div id="share">
                    <jdoc:include type="modules" name="share"  style="xhtml" />
                </div>

                <nav><jdoc:include type="modules" name="menu"  style="xhtml" /></nav>

                <aside id="lateral">
                    <jdoc:include type="modules" name="buscador"  style="xhtml" />
                    <jdoc:include type="modules" name="publicidad"  style="xhtml" />
                    <jdoc:include type="modules" name="videos"  style="xhtml" />
                </aside>

                <section id="content-all">
                    <jdoc:include type="message" />
                     <jdoc:include type="component" />
                     <jdoc:include type="modules" name="like"  style="xhtml" />
                </section>

                <section id="clientes">
                    <jdoc:include type="modules" name="clientes"  style="xhtml" />
                </section>
            </main>
        </div>

        <footer>
            <div id="footer-content">
                <address><jdoc:include type="modules" name="datos"  style="xhtml" /></address>
                <div id="redes"><jdoc:include type="modules" name="redes"  style="xhtml" /></div>
                <div id="sainet_copy">
                Sitio web desarrollado por
                    <a target="_blank" href="http://www.creandopaginasweb.com/">
                        <div id="logo-sainet"></div>
                    </a>
                </div>
            </div>
        </footer>
        <div class="global-notification"></div>
       <script type="text/javascript" src="js/load-scripts.php"></script>
	</body>
</html>
